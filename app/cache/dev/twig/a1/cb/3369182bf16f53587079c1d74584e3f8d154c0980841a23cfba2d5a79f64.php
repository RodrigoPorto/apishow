<?php

/* AdminBundle:User:new.html.twig */
class __TwigTemplate_a1cb3369182bf16f53587079c1d74584e3f8d154c0980841a23cfba2d5a79f64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Nuevo Usuario";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/admin/adminpage/bootstrap.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/admin/adminpage/main.css"), "html", null, true);
        echo "\" />

";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "<div class=\"navbar navbar-inverse navbar-static-top\">
      <div class=\"container\">
          <a class=\"navbar-brand\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("admin_logout");
        echo "\">Salir</a>
        </div>
    
 
   
      </div>
 
\t<!-- +++++ Welcome Section +++++ -->

\t    <div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-8 col-lg-offset-2 centered\">
\t\t\t\t\t<h4>Nuevo Usuario</h4>

                                           ";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

                           
                      
                            <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("user");
        echo "\">
                                Volver
                            </a>
                       
\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t    </div>
    
";
    }

    public function getTemplateName()
    {
        return "AdminBundle:User:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 33,  75 => 29,  58 => 15,  54 => 13,  51 => 11,  44 => 7,  39 => 6,  36 => 5,  30 => 3,);
    }
}
