<?php

/* ApiBundle:Series:index.html.twig */
class __TwigTemplate_85f51e4d8f455441247de0132b330d441c27b84fc178536de09b421370b6c9fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Series";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/api/bootstrap.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/api/style.css"), "html", null, true);
        echo "\" />

";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo " 
";
        // line 13
        if (((isset($context["accion"]) ? $context["accion"] : $this->getContext($context, "accion")) == 0)) {
            // line 14
            echo "
<div class=\"navbar\">
      <div class=\"navbar-inner\">
        <div class=\"container\"> <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\"> 
            <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> 
            </a> <a class=\"brand\" href=\"";
            // line 19
            echo $this->env->getExtension('routing')->getPath("api_homepage");
            echo "\"><img class=\"profile-img\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/api/usuario.png"), "html", null, true);
            echo "\"></a>
          <ul class=\"nav nav-collapse pull-right\">
            <li><a href=\"";
            // line 21
            echo $this->env->getExtension('routing')->getPath("api_homepage");
            echo "\" > Novedades </a></li>
            <li><a href=\"";
            // line 22
            echo $this->env->getExtension('routing')->getPath("api_peliculas");
            echo "\" > Peliculas</a></li>
            <li><a href=\"";
            // line 23
            echo $this->env->getExtension('routing')->getPath("api_series");
            echo "\" class=\"active\">Series</a></li>
            <li><a href=\"";
            // line 24
            echo $this->env->getExtension('routing')->getPath("front_logout");
            echo "\">Cerrar Sesión</a></li>
          </ul>
        </div>
      </div>
</div>

<div class=\"container profile\">
      <div class=\"span10\">
        <div class=\"span10 buscar\">
          <form action=\"";
            // line 33
            echo $this->env->getExtension('routing')->getPath("api_buscar_series");
            echo "\" method=\"POST\" class=\"form-search\">
                  <input   type=\"text\" class=\"input-mini search-query\" name=\"serie\" >
          <input  type=\"submit\" class=\"btn btn-success\" name=\"buscador\" value=\"Buscar\">
          </form>
        </div>
       </div>  
</div>

";
        } else {
            // line 42
            echo "<div class=\"navbar\">
     <div class=\"navbar-inner\">
        <div class=\"container\"> <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\"> 
                <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> 
                <a class=\"brand\" href=\"";
            // line 46
            echo $this->env->getExtension('routing')->getPath("api_homepage");
            echo "\"><img class=\"profile-img\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/api/usuario.png"), "html", null, true);
            echo "\"></a>
                <ul class=\"nav nav-collapse pull-right\">
                    <li><a href=\"";
            // line 48
            echo $this->env->getExtension('routing')->getPath("api_homepage");
            echo "\">Novedades </a></li>
                    <li><a href=\"";
            // line 49
            echo $this->env->getExtension('routing')->getPath("api_peliculas");
            echo "\">Peliculas</a></li>
                    <li><a href=\"";
            // line 50
            echo $this->env->getExtension('routing')->getPath("api_series");
            echo "\" class=\"active\">Series</a></li>
                    <li><a href=\"";
            // line 51
            echo $this->env->getExtension('routing')->getPath("front_logout");
            echo "\">Cerrar Sesión</a></li>
                </ul> 
         </div>
      </div>
</div>
<div class=\"container\">
    <div class=\"row buscar\">
        <div class=\"span10\">
              <form action=\"";
            // line 59
            echo $this->env->getExtension('routing')->getPath("api_buscar_series");
            echo "\" method=\"POST\" class=\"form-search\">
                <input type=\"text\" class=\"search-query\" name=\"serie\">
                 <input  type=\"submit\" name=\"buscador\" class=\"btn btn-success\" value=\"Buscar\">
              </form>
         </div>
    </div>
        
    <div class=\"row titulo\">
          <div class=\"span6\">
              <h2>";
            // line 68
            echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : $this->getContext($context, "titulo")), "html", null, true);
            echo "</h2>
          </div>
     </div>    
        
     <div class=\"container-fluid\">
          <div class=\"row-fluid\">
                <div class=\"span3\">
                     <img src=\"";
            // line 75
            echo twig_escape_filter($this->env, (isset($context["imagen"]) ? $context["imagen"] : $this->getContext($context, "imagen")), "html", null, true);
            echo "\">
                 </div>
                 <div class=\"span7\">
                    ";
            // line 78
            echo twig_escape_filter($this->env, (isset($context["descripcion"]) ? $context["descripcion"] : $this->getContext($context, "descripcion")), "html", null, true);
            echo "
                  </div>
 
                <div class=\"span2\">
                 <p> Puntuacion: ";
            // line 82
            echo twig_escape_filter($this->env, (isset($context["votos"]) ? $context["votos"] : $this->getContext($context, "votos")), "html", null, true);
            echo "</p><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/api/big58.png"), "html", null, true);
            echo "\">
                </div>
                <div class=\"span2\">
                  <p> Temporadas:  ";
            // line 85
            echo twig_escape_filter($this->env, (isset($context["temporadas"]) ? $context["temporadas"] : $this->getContext($context, "temporadas")), "html", null, true);
            echo " </p>
                 </div>
                <div class=\"span2\">
                  <p> Creador:  ";
            // line 88
            echo twig_escape_filter($this->env, (isset($context["creador"]) ? $context["creador"] : $this->getContext($context, "creador")), "html", null, true);
            echo " </p>
                 </div>
          </div>
     </div>
      
</div>
       
      
";
        }
        // line 97
        echo "        
";
    }

    // line 100
    public function block_javascripts($context, array $blocks = array())
    {
        // line 101
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/js/jquery.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/js/bootstrap.js"), "html", null, true);
        echo "\"></script>

";
    }

    public function getTemplateName()
    {
        return "ApiBundle:Series:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 102,  215 => 101,  212 => 100,  207 => 97,  195 => 88,  189 => 85,  181 => 82,  174 => 78,  168 => 75,  158 => 68,  146 => 59,  135 => 51,  131 => 50,  127 => 49,  123 => 48,  116 => 46,  110 => 42,  98 => 33,  86 => 24,  82 => 23,  78 => 22,  74 => 21,  67 => 19,  60 => 14,  58 => 13,  55 => 12,  52 => 11,  45 => 7,  40 => 6,  37 => 5,  31 => 3,);
    }
}
