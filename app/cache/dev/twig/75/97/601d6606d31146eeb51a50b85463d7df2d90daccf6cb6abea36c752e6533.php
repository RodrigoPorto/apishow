<?php

/* WebProfilerBundle:Profiler:toolbar.css.twig */
class __TwigTemplate_7597601d6606d31146eeb51a50b85463d7df2d90daccf6cb6abea36c752e6533 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ".sf-minitoolbar {
    display: none;

    position: fixed;
    bottom: 0;
    right: 0;

    padding: 5px 5px 0;

    background-color: #f7f7f7;
    background-image: -moz-linear-gradient(top, #e4e4e4, #ffffff);
    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#e4e4e4), to(#ffffff));
    background-image: -o-linear-gradient(top, #e4e4e4, #ffffff);
    background: linear-gradient(top, #e4e4e4, #ffffff);

    border-radius: 16px 0 0 0;

    z-index: 6000000;
}

.sf-toolbarreset * {
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
}

.sf-toolbarreset {
    position: fixed;
    background-color: #f7f7f7;
    left: 0;
    right: 0;
    height: 38px;
    margin: 0;
    padding: 0 40px 0 0;
    z-index: 6000000;
    font: 11px Verdana, Arial, sans-serif;
    text-align: left;
    color: #2f2f2f;

    background-image: -moz-linear-gradient(top, #e4e4e4, #ffffff);
    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#e4e4e4), to(#ffffff));
    background-image: -o-linear-gradient(top, #e4e4e4, #ffffff);
    background: linear-gradient(top, #e4e4e4, #ffffff);

    bottom: 0;
    border-top: 1px solid #bbb;
}
.sf-toolbarreset abbr {
    border-bottom: 1px dotted #000000;
    cursor: help;
}
.sf-toolbarreset span,
.sf-toolbarreset div {
    font-size: 11px;
}
.sf-toolbarreset img {
    width: auto;
    display: inline;
}

.sf-toolbarreset .hide-button {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    width: 40px;
    height: 40px;
    cursor: pointer;
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAMAAAAMCGV4AAAAllBMVEXDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PExMTPz8/Q0NDR0dHT09Pb29vc3Nzf39/h4eHi4uLj4+P6+vr7+/v8/Pz9/f3///+Nh2QuAAAAIXRSTlMABgwPGBswMzk8QktRV4SKjZOWmaKlq7TAxszb3urt+fy1vNEpAAAAiklEQVQIHUXBBxKCQBREwRFzDqjoGh+C2YV//8u5Sll2S0E/dof1tKdKM6GyqCto7PjZRJIS/mbSELgXOSd/BzpKIH1ZefVWpDDTHsi8mZVnwImPi5ndCLbaAZk3M58Bay0h9VbeSvMpjDUAHj4jL55AW1rxN5fU2PLjIgVRzNdxVFOlGzvnJi0Fb1XNGBHA9uQOAAAAAElFTkSuQmCC');
    background-repeat: no-repeat;
    background-position: 13px 11px;
}

.sf-toolbar-block {
    white-space: nowrap;
    color: #2f2f2f;
    display: block;
    min-height: 28px;
    border-right: 1px solid #e4e4e4;
    padding: 0;
    float: left;
    cursor: default;
}

.sf-toolbar-block span {
    display: inline-block;
}

.sf-toolbar-block .sf-toolbar-info-piece {
    line-height: 19px;
    margin-bottom: 5px;
}

.sf-toolbar-block .sf-toolbar-info-piece .sf-toolbar-status {
    padding: 0px 5px;
    border-radius: 5px;
    margin-bottom: 0px;
    vertical-align: top;
}

.sf-toolbar-block .sf-toolbar-info-piece:last-child {
    margin-bottom: 0;
}

.sf-toolbar-block .sf-toolbar-info-piece a,
.sf-toolbar-block .sf-toolbar-info-piece abbr {
    color: #2f2f2f;
}

.sf-toolbar-block .sf-toolbar-info-piece b {
    display: inline-block;
    width: 110px;
    vertical-align: top;
}

.sf-toolbar-block .sf-toolbar-info-with-next-pointer:after {
    content: ' :: ';
    color: #999;
}

.sf-toolbar-block .sf-toolbar-info-with-delimiter {
    border-right: 1px solid #999;
    padding-right: 5px;
    margin-right: 5px;
}

.sf-toolbar-block .sf-toolbar-info {
    display: none;
    position: absolute;
    background-color: #fff;
    border: 1px solid #bbb;
    padding: 9px 0;
    margin-left: -1px;

    bottom: 38px;
    border-bottom-width: 0;
    border-bottom: 1px solid #bbb;
    border-radius: 4px 4px 0 0;
}

.sf-toolbarreset > div:last-of-type .sf-toolbar-info {
    right: -1px;
}

.sf-toolbar-block .sf-toolbar-info:empty {
    visibility: hidden;
}

.sf-toolbar-block .sf-toolbar-status {
    display: inline-block;
    color: #fff;
    background-color: #666;
    padding: 3px 6px;
    border-radius: 3px;
    margin-bottom: 2px;
    vertical-align: middle;
    min-width: 6px;
    min-height: 13px;
}

.sf-toolbar-block .sf-toolbar-status abbr {
    color: #fff;
    border-bottom: 1px dotted black;
}

.sf-toolbar-block .sf-toolbar-status-green {
    background-color: #759e1a;
}

.sf-toolbar-block .sf-toolbar-status-red {
    background-color: #a33;
}

.sf-toolbar-block .sf-toolbar-status-yellow {
    background-color: #ffcc00;
    color: #000;
}

.sf-toolbar-block .sf-toolbar-status-black {
    background-color: #000;
}

.sf-toolbar-block .sf-toolbar-icon {
    display: block;
}

.sf-toolbar-block .sf-toolbar-icon > a,
.sf-toolbar-block .sf-toolbar-icon > span {
    display: block;
    font-weight: normal;
    text-decoration: none;
    margin: 0;
    padding: 5px 8px;
    min-width: 20px;
    text-align: center;
    vertical-align: middle;
}

.sf-toolbar-block .sf-toolbar-icon > a,
.sf-toolbar-block .sf-toolbar-icon > a:link
.sf-toolbar-block .sf-toolbar-icon > a:hover {
    color: black !important;
}

.sf-toolbar-block .sf-toolbar-icon > a[href]:after {
    content: \"\";
}

.sf-toolbar-block .sf-toolbar-icon img {
    border-width: 0;
    vertical-align: middle;
}

.sf-toolbar-block .sf-toolbar-icon img + span {
    margin-left: 5px;
    margin-top: 2px;
}

.sf-toolbar-block .sf-toolbar-icon .sf-toolbar-status {
    border-radius: 12px;
    border-bottom-left-radius: 0;
    margin-top: 0;
}

.sf-toolbar-block .sf-toolbar-info-method {
    border-bottom: 1px dashed #ccc;
    cursor: help;
}

.sf-toolbar-block .sf-toolbar-info-method[onclick=\"\"] {
    border-bottom: none;
    cursor: inherit;
}

.sf-toolbar-info-php {}
.sf-toolbar-info-php-ext {}

.sf-toolbar-info-php-ext .sf-toolbar-status {
    margin-left: 2px;
}

.sf-toolbar-info-php-ext .sf-toolbar-status:first-child {
    margin-left: 0;
}

.sf-toolbar-block a .sf-toolbar-info-piece-additional,
.sf-toolbar-block a .sf-toolbar-info-piece-additional-detail {
    display: inline-block;
}

.sf-toolbar-block a .sf-toolbar-info-piece-additional:empty,
.sf-toolbar-block a .sf-toolbar-info-piece-additional-detail:empty {
    display: none;
}

.sf-toolbarreset:hover {
    box-shadow: rgba(0, 0, 0, 0.3) 0 0 50px;
}

.sf-toolbar-block:hover {
    box-shadow: rgba(0, 0, 0, 0.35) 0 0 5px;
    border-right: none;
    margin-right: 1px;
    position: relative;
}

.sf-toolbar-block:hover .sf-toolbar-icon {
    background-color: #fff;
    border-top: 1px dotted #DDD;
    position: relative;
    margin-top: -1px;
    z-index: 10002;
}

.sf-toolbar-block:hover .sf-toolbar-info {
    display: block;
    min-width: -webkit-calc(100% + 2px);
    min-width: calc(100% + 2px);
    z-index: 10001;
    box-sizing: border-box;
    padding: 9px;
    line-height: 19px;
}

/***** Override the setting when the toolbar is on the top *****/
";
        // line 286
        if (($this->getContext($context, "position") == "top")) {
            // line 287
            echo "    .sf-minitoolbar {
        top: 0;
        bottom: auto;
        right: 0;

        background-color: #f7f7f7;

        background-image: -moz-linear-gradient(225deg, #e4e4e4, #ffffff);
        background-image: -webkit-gradient(linear, 100% 0%, 0% 0%, from(#e4e4e4), to(#ffffff));
        background-image: -o-linear-gradient(135deg, #e4e4e4, #ffffff);
        background: linear-gradient(225deg, #e4e4e4, #ffffff);

        border-radius: 0 0 0 16px;
    }

    .sf-toolbarreset {
        background-image: -moz-linear-gradient(225deg, #e4e4e4, #ffffff);
        background-image: -webkit-gradient(linear, 100% 0%, 0% 0%, from(#e4e4e4), to(#ffffff));
        background-image: -o-linear-gradient(135deg, #e4e4e4, #ffffff);
        background: linear-gradient(225deg, #e4e4e4, #ffffff);

        top: 0;
        bottom: auto;
        border-bottom: 1px solid #bbb;
    }

    .sf-toolbar-block .sf-toolbar-info {
        top: 39px;
        bottom: auto;
        border-top-width: 0;
        border-radius: 0 0 4px 4px;
    }

    .sf-toolbar-block:hover .sf-toolbar-icon {
        border-top: none;
        border-bottom: 1px dotted #DDD;
        margin-top: 0;
        margin-bottom: -1px;
    }
";
        }
        // line 327
        echo "
";
        // line 328
        if ((!$this->getContext($context, "floatable"))) {
            // line 329
            echo "    .sf-toolbarreset {
        position: static;
        background: #cbcbcb;

        background-image: -moz-linear-gradient(90deg, #cbcbcb, #e8e8e8) !important;
        background-image: -webkit-gradient(linear, 0% 0%, 100% 0%, from(#cbcbcb), to(#e8e8e8)) !important;
        background-image: -o-linear-gradient(180deg, #cbcbcb, #e8e8e8) !important;
        background: linear-gradient(90deg, #cbcbcb, #e8e8e8) !important;
    }
";
        }
        // line 339
        echo "
/***** Media query *****/
@media screen and (max-width: 779px) {
    .sf-toolbar-block .sf-toolbar-icon > * > :first-child ~ * {
        display: none;
    }

    .sf-toolbar-block .sf-toolbar-icon > * > .sf-toolbar-info-piece-additional,
    .sf-toolbar-block .sf-toolbar-icon > * > .sf-toolbar-info-piece-additional-detail {
        display: none !important;
    }
}

@media screen and (min-width: 880px) {
    .sf-toolbar-block .sf-toolbar-icon a[href\$=\"config\"] .sf-toolbar-info-piece-additional {
        display: inline-block;
    }
}

@media screen and (min-width: 980px) {
    .sf-toolbar-block .sf-toolbar-icon a[href\$=\"security\"] .sf-toolbar-info-piece-additional {
        display: inline-block;
    }
}

@media screen and (max-width: 1179px) {
    .sf-toolbar-block .sf-toolbar-icon > * > .sf-toolbar-info-piece-additional {
        display: none;
    }
}

@media screen and (max-width: 1439px) {
    .sf-toolbar-block .sf-toolbar-icon > * > .sf-toolbar-info-piece-additional-detail {
        display: none;
    }
}

/***** Media query print: Do not print the Toolbar. *****/
@media print {
    .sf-toolbar {
        display: none;
        visibility: hidden;
    }
}
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  810 => 492,  792 => 488,  788 => 486,  775 => 485,  706 => 473,  702 => 472,  698 => 471,  690 => 469,  686 => 468,  682 => 467,  679 => 466,  677 => 465,  660 => 464,  634 => 456,  629 => 454,  625 => 453,  622 => 452,  620 => 451,  606 => 449,  549 => 411,  532 => 410,  522 => 406,  389 => 160,  386 => 159,  378 => 157,  367 => 339,  358 => 151,  345 => 147,  340 => 145,  302 => 125,  296 => 121,  357 => 123,  327 => 114,  318 => 111,  306 => 286,  291 => 102,  274 => 110,  462 => 202,  449 => 198,  446 => 197,  431 => 189,  429 => 188,  394 => 168,  380 => 158,  373 => 156,  361 => 152,  348 => 140,  325 => 129,  323 => 128,  320 => 127,  315 => 131,  303 => 106,  300 => 105,  270 => 102,  267 => 101,  262 => 98,  172 => 57,  110 => 43,  14 => 1,  23 => 1,  170 => 84,  167 => 76,  113 => 48,  155 => 47,  97 => 52,  100 => 39,  20 => 1,  232 => 88,  213 => 78,  185 => 74,  175 => 58,  118 => 49,  65 => 11,  58 => 18,  34 => 5,  1361 => 391,  1352 => 390,  1331 => 384,  1324 => 383,  1322 => 382,  1319 => 381,  1296 => 377,  1269 => 375,  1266 => 374,  1254 => 369,  1249 => 368,  1247 => 367,  1244 => 366,  1235 => 360,  1229 => 358,  1226 => 357,  1221 => 356,  1216 => 354,  1209 => 349,  1200 => 347,  1196 => 346,  1193 => 345,  1190 => 344,  1188 => 343,  1185 => 342,  1177 => 338,  1160 => 330,  1155 => 328,  1152 => 327,  1143 => 322,  1141 => 321,  1098 => 313,  1095 => 312,  1079 => 306,  1077 => 305,  1074 => 304,  1067 => 299,  1064 => 298,  1056 => 293,  1053 => 292,  1051 => 291,  1048 => 290,  1040 => 285,  1032 => 283,  1029 => 282,  1027 => 281,  1024 => 280,  1016 => 276,  1014 => 272,  1004 => 266,  982 => 261,  979 => 260,  970 => 257,  967 => 256,  964 => 255,  961 => 254,  958 => 253,  952 => 251,  950 => 250,  939 => 243,  936 => 242,  934 => 241,  931 => 240,  923 => 236,  918 => 234,  915 => 233,  903 => 229,  897 => 227,  894 => 226,  892 => 225,  889 => 224,  881 => 220,  878 => 219,  876 => 218,  873 => 217,  865 => 213,  857 => 210,  849 => 206,  846 => 205,  844 => 204,  841 => 203,  833 => 199,  830 => 198,  828 => 197,  825 => 196,  817 => 192,  812 => 190,  809 => 189,  801 => 185,  798 => 184,  796 => 489,  793 => 182,  772 => 172,  769 => 171,  764 => 169,  756 => 165,  753 => 164,  751 => 163,  749 => 479,  746 => 478,  739 => 156,  729 => 155,  724 => 154,  721 => 153,  715 => 151,  710 => 475,  697 => 141,  695 => 139,  689 => 137,  683 => 135,  680 => 134,  678 => 133,  675 => 132,  666 => 126,  649 => 462,  640 => 119,  635 => 117,  619 => 113,  614 => 111,  598 => 107,  596 => 106,  593 => 105,  576 => 101,  564 => 99,  557 => 96,  550 => 94,  547 => 93,  529 => 409,  527 => 408,  524 => 90,  503 => 81,  496 => 79,  493 => 78,  490 => 77,  478 => 74,  470 => 73,  459 => 69,  456 => 68,  450 => 64,  442 => 62,  433 => 60,  426 => 58,  414 => 52,  408 => 176,  405 => 49,  403 => 48,  377 => 37,  366 => 33,  350 => 327,  342 => 137,  332 => 116,  308 => 287,  299 => 8,  293 => 120,  290 => 119,  288 => 118,  281 => 114,  250 => 341,  248 => 97,  245 => 87,  222 => 83,  210 => 77,  202 => 94,  191 => 67,  134 => 39,  124 => 62,  76 => 27,  1871 => 658,  1869 => 657,  1867 => 656,  1864 => 655,  1859 => 652,  1848 => 650,  1843 => 649,  1840 => 648,  1837 => 647,  1828 => 642,  1825 => 641,  1821 => 639,  1815 => 637,  1812 => 636,  1809 => 635,  1806 => 634,  1800 => 632,  1794 => 630,  1791 => 629,  1788 => 626,  1768 => 624,  1765 => 623,  1762 => 622,  1759 => 621,  1756 => 620,  1753 => 619,  1751 => 618,  1748 => 617,  1745 => 616,  1742 => 615,  1739 => 614,  1737 => 613,  1734 => 612,  1730 => 611,  1728 => 610,  1725 => 609,  1718 => 602,  1712 => 600,  1706 => 598,  1704 => 597,  1702 => 596,  1699 => 595,  1692 => 588,  1688 => 586,  1681 => 584,  1678 => 580,  1674 => 579,  1669 => 578,  1666 => 577,  1659 => 575,  1656 => 570,  1651 => 569,  1648 => 568,  1645 => 567,  1642 => 566,  1627 => 564,  1624 => 563,  1620 => 562,  1617 => 561,  1599 => 560,  1597 => 559,  1595 => 558,  1592 => 557,  1584 => 551,  1582 => 550,  1580 => 549,  1578 => 548,  1575 => 547,  1566 => 543,  1561 => 541,  1559 => 540,  1556 => 539,  1548 => 534,  1542 => 532,  1540 => 531,  1520 => 530,  1517 => 529,  1509 => 523,  1506 => 522,  1500 => 520,  1497 => 519,  1494 => 518,  1491 => 517,  1486 => 515,  1483 => 514,  1480 => 513,  1478 => 512,  1475 => 511,  1466 => 506,  1463 => 505,  1460 => 504,  1457 => 503,  1455 => 502,  1452 => 501,  1443 => 497,  1441 => 496,  1438 => 495,  1431 => 489,  1426 => 488,  1424 => 487,  1421 => 486,  1413 => 479,  1407 => 477,  1405 => 476,  1402 => 475,  1398 => 473,  1396 => 472,  1386 => 470,  1380 => 468,  1378 => 467,  1375 => 466,  1372 => 465,  1369 => 464,  1367 => 463,  1364 => 462,  1362 => 461,  1356 => 459,  1350 => 389,  1347 => 388,  1345 => 455,  1342 => 454,  1333 => 450,  1327 => 448,  1323 => 447,  1318 => 446,  1312 => 443,  1309 => 442,  1306 => 441,  1304 => 440,  1301 => 439,  1294 => 436,  1291 => 435,  1283 => 431,  1279 => 430,  1274 => 429,  1271 => 376,  1265 => 424,  1259 => 422,  1256 => 421,  1250 => 419,  1248 => 418,  1240 => 417,  1237 => 416,  1234 => 415,  1228 => 411,  1222 => 409,  1219 => 355,  1213 => 406,  1211 => 405,  1205 => 404,  1202 => 403,  1199 => 402,  1192 => 399,  1189 => 398,  1181 => 392,  1175 => 337,  1172 => 336,  1166 => 332,  1163 => 386,  1157 => 329,  1154 => 383,  1148 => 381,  1146 => 380,  1142 => 379,  1140 => 378,  1124 => 377,  1121 => 376,  1118 => 320,  1115 => 319,  1112 => 318,  1109 => 317,  1106 => 316,  1103 => 315,  1100 => 314,  1097 => 368,  1094 => 367,  1091 => 366,  1088 => 308,  1086 => 364,  1084 => 307,  1081 => 362,  1069 => 358,  1066 => 357,  1063 => 356,  1060 => 355,  1058 => 354,  1055 => 353,  1046 => 346,  1043 => 345,  1036 => 284,  1033 => 341,  1030 => 340,  1028 => 339,  1021 => 338,  1015 => 336,  1012 => 271,  1009 => 270,  1007 => 333,  1005 => 332,  1003 => 331,  1000 => 330,  992 => 326,  989 => 325,  987 => 324,  984 => 323,  976 => 259,  973 => 258,  971 => 317,  968 => 316,  959 => 310,  955 => 252,  951 => 308,  947 => 249,  943 => 306,  938 => 305,  935 => 304,  932 => 303,  926 => 301,  920 => 235,  912 => 297,  900 => 228,  891 => 295,  888 => 294,  885 => 293,  883 => 292,  880 => 291,  871 => 286,  868 => 285,  862 => 212,  860 => 211,  856 => 281,  851 => 280,  848 => 279,  845 => 278,  838 => 275,  832 => 272,  826 => 271,  814 => 191,  807 => 491,  804 => 268,  802 => 267,  800 => 266,  797 => 265,  789 => 260,  785 => 178,  783 => 177,  782 => 256,  781 => 255,  780 => 176,  777 => 253,  774 => 252,  767 => 170,  761 => 246,  755 => 245,  737 => 244,  730 => 243,  727 => 476,  725 => 241,  723 => 240,  720 => 239,  712 => 150,  709 => 233,  707 => 148,  699 => 142,  696 => 140,  694 => 470,  681 => 225,  673 => 223,  671 => 222,  668 => 221,  664 => 219,  662 => 125,  658 => 124,  656 => 215,  654 => 123,  651 => 213,  645 => 209,  643 => 120,  638 => 118,  632 => 206,  628 => 204,  626 => 203,  621 => 201,  617 => 112,  601 => 446,  595 => 197,  592 => 196,  587 => 195,  584 => 194,  581 => 193,  578 => 192,  575 => 191,  572 => 190,  570 => 189,  567 => 414,  558 => 183,  555 => 95,  546 => 180,  541 => 179,  538 => 178,  523 => 176,  520 => 175,  517 => 404,  515 => 85,  512 => 84,  509 => 83,  506 => 170,  504 => 169,  501 => 80,  498 => 163,  495 => 162,  492 => 161,  489 => 160,  486 => 159,  484 => 158,  481 => 157,  467 => 72,  464 => 71,  458 => 147,  447 => 143,  443 => 142,  439 => 195,  432 => 140,  428 => 59,  421 => 137,  418 => 136,  411 => 132,  400 => 47,  395 => 125,  388 => 42,  364 => 112,  359 => 110,  356 => 109,  351 => 120,  346 => 107,  343 => 146,  329 => 131,  316 => 16,  280 => 82,  277 => 2,  271 => 374,  266 => 1,  260 => 363,  257 => 74,  225 => 298,  195 => 65,  253 => 100,  207 => 75,  198 => 52,  190 => 76,  104 => 32,  338 => 135,  335 => 134,  333 => 135,  326 => 138,  321 => 135,  311 => 14,  304 => 118,  301 => 89,  297 => 104,  286 => 112,  275 => 105,  265 => 105,  261 => 104,  255 => 101,  237 => 65,  233 => 87,  216 => 79,  211 => 41,  206 => 55,  200 => 72,  188 => 90,  186 => 239,  180 => 56,  174 => 65,  153 => 77,  150 => 55,  129 => 64,  126 => 39,  81 => 23,  70 => 24,  53 => 15,  445 => 126,  441 => 196,  438 => 114,  424 => 114,  422 => 184,  419 => 109,  415 => 180,  412 => 106,  406 => 130,  401 => 172,  390 => 43,  385 => 41,  382 => 92,  376 => 90,  371 => 156,  363 => 153,  353 => 328,  349 => 80,  347 => 77,  344 => 119,  336 => 87,  334 => 141,  331 => 140,  328 => 139,  324 => 113,  317 => 67,  313 => 15,  310 => 92,  307 => 128,  295 => 87,  292 => 86,  289 => 113,  279 => 54,  276 => 111,  263 => 95,  259 => 103,  256 => 96,  251 => 99,  249 => 96,  244 => 50,  239 => 49,  236 => 48,  234 => 64,  231 => 83,  226 => 84,  223 => 80,  218 => 59,  215 => 280,  212 => 78,  197 => 69,  194 => 68,  192 => 90,  184 => 63,  181 => 65,  178 => 59,  165 => 83,  161 => 63,  152 => 46,  148 => 69,  146 => 50,  137 => 65,  127 => 35,  114 => 44,  90 => 42,  84 => 27,  77 => 21,  480 => 75,  474 => 153,  469 => 158,  461 => 70,  457 => 153,  453 => 199,  444 => 149,  440 => 148,  437 => 61,  435 => 146,  430 => 144,  427 => 116,  423 => 57,  413 => 134,  409 => 131,  407 => 131,  402 => 130,  398 => 100,  393 => 124,  387 => 164,  384 => 121,  381 => 119,  379 => 118,  374 => 36,  368 => 34,  365 => 111,  362 => 111,  360 => 77,  355 => 329,  341 => 118,  337 => 22,  322 => 96,  314 => 122,  312 => 130,  309 => 129,  305 => 95,  298 => 120,  294 => 124,  285 => 3,  283 => 115,  278 => 98,  268 => 373,  264 => 84,  258 => 94,  252 => 72,  247 => 78,  241 => 93,  229 => 87,  220 => 81,  214 => 42,  177 => 54,  169 => 210,  140 => 58,  132 => 65,  128 => 60,  107 => 52,  61 => 12,  273 => 380,  269 => 107,  254 => 100,  243 => 92,  240 => 326,  238 => 312,  235 => 89,  230 => 303,  227 => 86,  224 => 81,  221 => 79,  219 => 78,  217 => 77,  208 => 76,  204 => 267,  179 => 85,  159 => 196,  143 => 51,  135 => 13,  119 => 40,  102 => 33,  71 => 23,  67 => 22,  63 => 21,  59 => 16,  38 => 12,  94 => 21,  89 => 30,  85 => 23,  75 => 24,  68 => 12,  56 => 16,  87 => 32,  21 => 1,  26 => 6,  93 => 31,  88 => 25,  78 => 18,  46 => 13,  27 => 7,  44 => 11,  31 => 8,  28 => 3,  201 => 53,  196 => 92,  183 => 58,  171 => 81,  166 => 54,  163 => 82,  158 => 80,  156 => 62,  151 => 59,  142 => 59,  138 => 69,  136 => 71,  121 => 50,  117 => 39,  105 => 25,  91 => 33,  62 => 27,  49 => 14,  24 => 18,  25 => 3,  19 => 1,  79 => 18,  72 => 18,  69 => 17,  47 => 21,  40 => 6,  37 => 7,  22 => 17,  246 => 136,  157 => 33,  145 => 74,  139 => 49,  131 => 45,  123 => 61,  120 => 31,  115 => 16,  111 => 47,  108 => 33,  101 => 31,  98 => 34,  96 => 30,  83 => 31,  74 => 20,  66 => 18,  55 => 38,  52 => 12,  50 => 18,  43 => 12,  41 => 19,  35 => 9,  32 => 4,  29 => 3,  209 => 72,  203 => 73,  199 => 93,  193 => 64,  189 => 66,  187 => 75,  182 => 87,  176 => 86,  173 => 85,  168 => 61,  164 => 46,  162 => 59,  154 => 60,  149 => 51,  147 => 75,  144 => 42,  141 => 73,  133 => 55,  130 => 46,  125 => 42,  122 => 41,  116 => 57,  112 => 36,  109 => 52,  106 => 51,  103 => 34,  99 => 23,  95 => 34,  92 => 28,  86 => 45,  82 => 19,  80 => 27,  73 => 33,  64 => 21,  60 => 14,  57 => 20,  54 => 19,  51 => 37,  48 => 16,  45 => 9,  42 => 11,  39 => 10,  36 => 10,  33 => 9,  30 => 5,);
    }
}
