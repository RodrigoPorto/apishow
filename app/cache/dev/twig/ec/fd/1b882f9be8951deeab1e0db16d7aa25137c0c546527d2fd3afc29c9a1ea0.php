<?php

/* AdminBundle:User:edit.html.twig */
class __TwigTemplate_ecfd1b882f9be8951deeab1e0db16d7aa25137c0c546527d2fd3afc29c9a1ea0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Editar";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/admin/adminpage/bootstrap.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/admin/adminpage/main.css"), "html", null, true);
        echo "\" />

";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "<div class=\"navbar navbar-inverse navbar-static-top\">
          <div class=\"container\">
               <a class=\"navbar-brand\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("admin_logout");
        echo "\">Salir</a>
            </div>
   </div>

    <div class=\"container\">
         <div class=\"row\">
             <div class=\"col-lg-8 col-lg-offset-2 centered\">
                 
                    <h1>Editar Usuario</h1>
                    
                     ";
        // line 25
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form');
        echo "

                     ";
        // line 27
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "

                <a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("user");
        echo "\">Volver</a>

            </div>
        </div>
   </div> 

";
    }

    public function getTemplateName()
    {
        return "AdminBundle:User:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 29,  76 => 27,  71 => 25,  58 => 15,  54 => 13,  51 => 11,  44 => 7,  39 => 6,  36 => 5,  30 => 3,);
    }
}
