<?php

/* WebProfilerBundle:Collector:time.html.twig */
class __TwigTemplate_246f96f6c4e30aa0dbca1d2c594c668f3fccf1a312bb74861dedb2b82b3d7058 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@WebProfiler/Profiler/layout.html.twig");

        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
            'panelContent' => array($this, 'block_panelContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["__internal_d7b2aaf80cdcbd6c9307ee6f9b48355eb4d876bc6b785f984828764a66494196"] = $this;
        // line 5
        if ((!array_key_exists("colors", $context))) {
            // line 6
            $context["colors"] = array("default" => "#aacd4e", "section" => "#666", "event_listener" => "#3dd", "event_listener_loading" => "#add", "template" => "#dd3", "doctrine" => "#d3d", "propel" => "#f4d", "child_sections" => "#eed");
        }
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 18
    public function block_toolbar($context, array $blocks = array())
    {
        // line 19
        echo "    ";
        $context["duration"] = ((twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "events"))) ? (sprintf("%.0f ms", $this->getAttribute($this->getContext($context, "collector"), "duration"))) : ("n/a"));
        // line 20
        echo "    ";
        ob_start();
        // line 21
        echo "        <img width=\"16\" height=\"28\" alt=\"Time\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAcCAYAAABoMT8aAAABqUlEQVR42t2Vv0sCYRyHX9OmEhsMx/YKGlwLQ69DTEUSBJEQEy5J3FRc/BsuiFqEIIcQIRo6ysUhoaBBWhoaGoJwiMJLglRKrs8bXgienmkQdPDAwX2f57j3fhFJkkbiPwTK5bIiFoul3kmPud8MqKMewDXpwuGww+12n9hsNhFnlijYf/Z4PDmO45Yxo+10ZFGTyWRMEItU6AdCx7lczkgd6n7J2Wx2xm63P6jJMk6n80YQBBN1aUDv9XqvlAbbm2LE7/cLODRB0un0VveAeoDC8/waCQQC18MGQqHQOcEKvw8bcLlcL6TfYnVtCrGRAlartUUYhmn1jKg/E3USjUYfhw3E4/F7ks/nz4YNFIvFQ/ogbUYikdefyqlU6gnuOg2YK5XKvs/n+xhUDgaDTVEUt+HO04ABOBA5isViDTU5kUi81Wq1AzhWMEkDGmAEq2C3UCjcYXGauDvfEsuyUjKZbJRKpVvM8IABU9SVX+cxYABmwIE9cFqtVi9xtgvsC2AHbIAFoKey0gdlHEyDObAEWLACFsEsMALdIJ80+dK0bTS95v7+v/AJnis0eO906QwAAAAASUVORK5CYII=\" />
        <span>";
        // line 22
        echo twig_escape_filter($this->env, $this->getContext($context, "duration"), "html", null, true);
        echo "</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 24
        echo "    ";
        ob_start();
        // line 25
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Total time</b>
            <span>";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "duration"), "html", null, true);
        echo "</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 30
        echo "    ";
        $this->env->loadTemplate("@WebProfiler/Profiler/toolbar_item.html.twig")->display(array_merge($context, array("link" => $this->getContext($context, "profiler_url"))));
    }

    // line 33
    public function block_menu($context, array $blocks = array())
    {
        // line 34
        echo "<span class=\"label\">
    <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAhCAYAAADOHBvaAAACz0lEQVR42t2XXWhSYRjHc+pyWrC10e66qKggiGoF0Qh1SBPFDxCcX00yrG6EImFsRhAuvUjwQgZB1EUICYEkgRJGB0QmKMNGEYx1URYEg2w6amWY/YUXXH7Ms9M5RAk/PByf8/58P573ec+2Wq32V/g3xFKpdB3UQBV8JVTJvXUuxbXN+P96TODNzMzsNJvNo3UCgYCYszmmKKrPYrE4NBrNU7lcvtY8xGNjYyWtVvvEZrPZES9kQyy02+3nx8fH3xFJV5RK5RuHw2GqP8tInEgk+g0GwyN6wlZMJtPdfD6/Y0viVCo1iKHLMpM20Ov1FNqT0BWLsWhiXdImDyiQ7ybHVN1HnKibWHgNHxr5egXI6t90ej49PX0BsYKO4qWlpT1qtXqFhlhGnpHREWPaPiB2dydxL/7ZTRLMnpjg8/lcpNct4mGr1fqSK7HT6UwhfleLuFwuH1EoFBWuxDqd7jPiDzSLe+Lx+AQJYl9MKBQKZ+pb7kaxKBKJXGUgPgqCgGrQOc3S6fQEfuNvFEtisdgUA3E/kcsadE6zhYUFG9lKG2LcvMxA3EKnKUBx+bm6uqptFvcCtUql+kECWcdoNH6BQw56fltcYNTtdr/nSuzxeJbhONkujw9Fo9EYV+JkMvkAjv3txEOVSuUSytk3tqWoz2to/xwYaCcWgtPI5+dsSrGo6mn0GG2PtG6ZDYbBRb/f/5EtcSgUeos2J8HgpmURHMaQz6JglP5U6vV6P6G962Sr5HcUE/rAKXB7bm5uhenwhsPheim8BUa6HwQaSMjSv5HNZl+gstDOb5fL9X1xcTGLZ6fAMSDe6ilTBA4CO7iXyWReB4PBMo5F1WYZymkVc1nK5XKvEHsHWMA+sJ3p8VYAhkjvJ4EPRMCzYrE4XyqV5nGdBA/BLLCC46Tu8tl4kxCAAdKLE0AGzhKkZB73kqIh4PI1lQf4BB6TNn4B8KR3FN9bp4MAAAAASUVORK5CYII=\" alt=\"Timeline\"></span>
    <strong>Timeline</strong>
</span>
";
    }

    // line 40
    public function block_panel($context, array $blocks = array())
    {
        // line 41
        echo "    <h2>Timeline</h2>
    ";
        // line 42
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "events"))) {
            // line 43
            echo "        ";
            $this->displayBlock("panelContent", $context, $blocks);
            echo "
    ";
        } else {
            // line 45
            echo "        <p>
            <em>No timing events have been recorded. Are you sure that debugging is enabled in the kernel?</em>
        </p>
    ";
        }
    }

    // line 51
    public function block_panelContent($context, array $blocks = array())
    {
        // line 52
        echo "    <form id=\"timeline-control\" action=\"\" method=\"get\">
        <input type=\"hidden\" name=\"panel\" value=\"time\">
        <table>
            <tr>
                <th style=\"width: 20%\">Total time</th>
                <td>";
        // line 57
        echo twig_escape_filter($this->env, sprintf("%.0f", $this->getAttribute($this->getContext($context, "collector"), "duration")), "html", null, true);
        echo " ms</td>
            </tr>
            <tr>
                <th>Initialization time</th>
                <td>";
        // line 61
        echo twig_escape_filter($this->env, sprintf("%.0f", $this->getAttribute($this->getContext($context, "collector"), "inittime")), "html", null, true);
        echo " ms</td>
            </tr>
            <tr>
                <th>Threshold</th>
                <td><input type=\"number\" size=\"3\" name=\"threshold\" value=\"1\" min=\"0\"> ms</td>
            </tr>
        </table>
    </form>

    <h3>
        ";
        // line 71
        echo (($this->getAttribute($this->getContext($context, "profile"), "parent")) ? ("Request") : ("Main Request"));
        echo "
        <small>
            - ";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "events"), "__section__"), "duration"), "html", null, true);
        echo " ms
            ";
        // line 74
        if ($this->getAttribute($this->getContext($context, "profile"), "parent")) {
            // line 75
            echo "                - <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_profiler", array("token" => $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "parent"), "token"), "panel" => "time")), "html", null, true);
            echo "\">parent</a>
            ";
        }
        // line 77
        echo "        </small>
    </h3>

    ";
        // line 80
        echo $context["__internal_d7b2aaf80cdcbd6c9307ee6f9b48355eb4d876bc6b785f984828764a66494196"]->getdisplay_timeline(("timeline_" . $this->getContext($context, "token")), $this->getAttribute($this->getContext($context, "collector"), "events"), $this->getContext($context, "colors"));
        echo "

    ";
        // line 82
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "children"))) {
            // line 83
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "profile"), "children"));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 84
                echo "            ";
                $context["events"] = $this->getAttribute($this->getAttribute($this->getContext($context, "child"), "getcollector", array(0 => "time"), "method"), "events");
                // line 85
                echo "            <h3>
                Sub-request \"<a href=\"";
                // line 86
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_profiler", array("token" => $this->getAttribute($this->getContext($context, "child"), "token"), "panel" => "time")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "child"), "getcollector", array(0 => "request"), "method"), "requestattributes"), "get", array(0 => "_controller"), "method"), "html", null, true);
                echo "</a>\"
                <small> - ";
                // line 87
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "events"), "__section__"), "duration"), "html", null, true);
                echo " ms</small>
            </h3>

            ";
                // line 90
                echo $context["__internal_d7b2aaf80cdcbd6c9307ee6f9b48355eb4d876bc6b785f984828764a66494196"]->getdisplay_timeline(("timeline_" . $this->getAttribute($this->getContext($context, "child"), "token")), $this->getContext($context, "events"), $this->getContext($context, "colors"));
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 92
            echo "    ";
        }
        // line 93
        echo "
    <script>";
        // line 94
        echo "//<![CDATA[
        /**
         * In-memory key-value cache manager
         */
        var cache = new function() {
            \"use strict\";
            var dict = {};

            this.get = function(key) {
                return dict.hasOwnProperty(key)
                    ? dict[key]
                    : null;
                }

            this.set = function(key, value) {
                dict[key] = value;

                return value;
            }
        };

        /**
         * Query an element with a CSS selector.
         *
         * @param  string selector a CSS-selector-compatible query string.
         *
         * @return DOMElement|null
         */
        function query(selector)
        {
            \"use strict\";
            var key = 'SELECTOR: ' + selector;

            return cache.get(key) || cache.set(key, document.querySelector(selector));
        }

        /**
         * Canvas Manager
         */
        function CanvasManager(requests, maxRequestTime) {
            \"use strict\";

            var _drawingColors  = ";
        // line 136
        echo twig_jsonencode_filter($this->getContext($context, "colors"));
        echo ",
                _storagePrefix  = 'timeline/',
                _threshold      = 1,
                _requests       = requests,
                _maxRequestTime = maxRequestTime;

            /**
             * Check whether this event is a child event.
             *
             * @return true if it is.
             */
            function isChildEvent(event)
            {
                return '__section__.child' === event.name;
            }

            /**
             * Check whether this event is categorized in 'section'.
             *
             * @return true if it is.
             */
            function isSectionEvent(event)
            {
                return 'section' === event.category;
            }

            /**
             * Get the width of the container.
             */
            function getContainerWidth()
            {
                return query('#collector-content h2').clientWidth;
            }

            /**
             * Draw one canvas.
             *
             * @param request   the request object
             * @param max       <subjected for removal>
             * @param threshold the threshold (lower bound) of the length of the timeline (in milliseconds).
             * @param width     the width of the canvas.
             */
            this.drawOne = function(request, max, threshold, width)
            {
                \"use strict\";
                var text,
                    ms,
                    xc,
                    drawableEvents,
                    mainEvents,
                    elementId    = 'timeline_' + request.id,
                    canvasHeight = 0,
                    gapPerEvent  = 38,
                    colors = _drawingColors,
                    space  = 10.5,
                    ratio  = (width - space * 2) / max,
                    h = space,
                    x = request.left * ratio + space, // position
                    canvas = cache.get(elementId) || cache.set(elementId, document.getElementById(elementId)),
                    ctx    = canvas.getContext(\"2d\"),
                    backingStoreRatio,
                    scaleRatio,
                    devicePixelRatio;

                // Filter events whose total time is below the threshold.
                drawableEvents = request.events.filter(function(event) {
                    return event.duration >= threshold;
                });

                canvasHeight += gapPerEvent * drawableEvents.length;

                // For retina displays so text and boxes will be crisp
                devicePixelRatio  = window.devicePixelRatio          == \"undefined\" ? 1 : window.devicePixelRatio;
                backingStoreRatio = ctx.webkitBackingStorePixelRatio == \"undefined\" ? 1 : ctx.webkitBackingStorePixelRatio;
                scaleRatio        = devicePixelRatio / 1;

                canvasHeight += gapPerEvent * drawableEvents.length;

                canvas.width  = width * scaleRatio;
                canvas.height = canvasHeight * scaleRatio;

                canvas.style.width = width + 'px';
                canvas.style.height = canvasHeight + 'px';

                ctx.scale(scaleRatio, scaleRatio);

                ctx.textBaseline = \"middle\";
                ctx.lineWidth = 0;

                // For each event, draw a line.
                ctx.strokeStyle = \"#dfdfdf\";

                drawableEvents.forEach(function(event) {
                    event.periods.forEach(function(period) {
                        var timelineHeadPosition = x + period.start * ratio;

                        if (isChildEvent(event)) {
                            ctx.fillStyle = colors.child_sections;
                            ctx.fillRect(timelineHeadPosition, 0, (period.end - period.start) * ratio, canvasHeight);
                        } else if (isSectionEvent(event)) {
                            var timelineTailPosition = x + period.end * ratio;

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, 0);
                            ctx.lineTo(timelineHeadPosition, canvasHeight);
                            ctx.moveTo(timelineTailPosition, 0);
                            ctx.lineTo(timelineTailPosition, canvasHeight);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();
                        }
                    });
                });

                // Filter for main events.
                mainEvents = drawableEvents.filter(function(event) {
                    return !isChildEvent(event)
                });

                // For each main event, draw the visual presentation of timelines.
                mainEvents.forEach(function(event) {

                    h += 8;

                    // For each sub event, ...
                    event.periods.forEach(function(period) {
                        // Set the drawing style.
                        ctx.fillStyle   = colors['default'];
                        ctx.strokeStyle = colors['default'];

                        if (colors[event.name]) {
                            ctx.fillStyle   = colors[event.name];
                            ctx.strokeStyle = colors[event.name];
                        } else if (colors[event.category]) {
                            ctx.fillStyle   = colors[event.category];
                            ctx.strokeStyle = colors[event.category];
                        }

                        // Draw the timeline
                        var timelineHeadPosition = x + period.start * ratio;

                        if (!isSectionEvent(event)) {
                            ctx.fillRect(timelineHeadPosition, h + 3, 2, 6);
                            ctx.fillRect(timelineHeadPosition, h, (period.end - period.start) * ratio || 2, 6);
                        } else {
                            var timelineTailPosition = x + period.end * ratio;

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, h);
                            ctx.lineTo(timelineHeadPosition, h + 11);
                            ctx.lineTo(timelineHeadPosition + 8, h);
                            ctx.lineTo(timelineHeadPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();

                            ctx.beginPath();
                            ctx.moveTo(timelineTailPosition, h);
                            ctx.lineTo(timelineTailPosition, h + 11);
                            ctx.lineTo(timelineTailPosition - 8, h);
                            ctx.lineTo(timelineTailPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, h);
                            ctx.lineTo(timelineTailPosition, h);
                            ctx.lineTo(timelineTailPosition, h + 2);
                            ctx.lineTo(timelineHeadPosition, h + 2);
                            ctx.lineTo(timelineHeadPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();
                        }
                    });

                    h += 30;

                    ctx.beginPath();
                    ctx.strokeStyle = \"#dfdfdf\";
                    ctx.moveTo(0, h - 10);
                    ctx.lineTo(width, h - 10);
                    ctx.closePath();
                    ctx.stroke();
                });

                h = space;

                // For each event, draw the label.
                mainEvents.forEach(function(event) {

                    ctx.fillStyle = \"#444\";
                    ctx.font = \"12px sans-serif\";
                    text = event.name;
                    ms = \" ~ \" + (event.duration < 1 ? event.duration : parseInt(event.duration, 10)) + \" ms / ~ \" + event.memory + \" MB\";
                    if (x + event.starttime * ratio + ctx.measureText(text + ms).width > width) {
                        ctx.textAlign = \"end\";
                        ctx.font = \"10px sans-serif\";
                        xc = x + event.endtime * ratio - 1;
                        ctx.fillText(ms, xc, h);

                        xc -= ctx.measureText(ms).width;
                        ctx.font = \"12px sans-serif\";
                        ctx.fillText(text, xc, h);
                    } else {
                        ctx.textAlign = \"start\";
                        ctx.font = \"12px sans-serif\";
                        xc = x + event.starttime * ratio + 1;
                        ctx.fillText(text, xc, h);

                        xc += ctx.measureText(text).width;
                        ctx.font = \"10px sans-serif\";
                        ctx.fillText(ms, xc, h);
                    }

                    h += gapPerEvent;
                });
            };

            this.drawAll = function(width, threshold)
            {
                \"use strict\";

                width     = width || getContainerWidth();
                threshold = threshold || this.getThreshold();

                var self = this;

                _requests.forEach(function(request) {
                    self.drawOne(request, maxRequestTime, threshold, width);
                });
            };

            this.getThreshold = function() {
                var threshold = Sfjs.getPreference(_storagePrefix + 'threshold');

                if (threshold === null) {
                    return _threshold;
                }

                _threshold = parseInt(threshold);

                return _threshold;
            };

            this.setThreshold = function(threshold)
            {
                _threshold = threshold;

                Sfjs.setPreference(_storagePrefix + 'threshold', threshold);

                return this;
            };
        };

        function canvasAutoUpdateOnResizeAndSubmit(e) {
            e.preventDefault();
            canvasManager.drawAll();
        }

        function canvasAutoUpdateOnThresholdChange(e) {
            canvasManager
                .setThreshold(query('input[name=\"threshold\"]').value)
                .drawAll();
        }

        var requests_data = {
            \"max\": ";
        // line 404
        echo twig_escape_filter($this->env, sprintf("%F", $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "events"), "__section__"), "endtime")), "js", null, true);
        echo ",
            \"requests\": [
";
        // line 406
        echo $context["__internal_d7b2aaf80cdcbd6c9307ee6f9b48355eb4d876bc6b785f984828764a66494196"]->getdump_request_data($this->getContext($context, "token"), $this->getContext($context, "profile"), $this->getAttribute($this->getContext($context, "collector"), "events"), $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "events"), "__section__"), "origin"));
        echo "

";
        // line 408
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "children"))) {
            // line 409
            echo "                ,
";
            // line 410
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "profile"), "children"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 411
                echo $context["__internal_d7b2aaf80cdcbd6c9307ee6f9b48355eb4d876bc6b785f984828764a66494196"]->getdump_request_data($this->getAttribute($this->getContext($context, "child"), "token"), $this->getContext($context, "child"), $this->getAttribute($this->getAttribute($this->getContext($context, "child"), "getcollector", array(0 => "time"), "method"), "events"), $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "events"), "__section__"), "origin"));
                echo (($this->getAttribute($this->getContext($context, "loop"), "last")) ? ("") : (","));
                echo "
";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 414
        echo "            ]
        };

        var canvasManager = new CanvasManager(requests_data.requests, requests_data.max);

        query('input[name=\"threshold\"]').value = canvasManager.getThreshold();
        canvasManager.drawAll();

        // Update the colors of legends.
        var timelineLegends = document.querySelectorAll('.sf-profiler-timeline > .legends > span[data-color]');

        for (var i = 0; i < timelineLegends.length; ++i) {
            var timelineLegend = timelineLegends[i];

            timelineLegend.style.borderLeftColor = timelineLegend.getAttribute('data-color');
        }

        // Bind event handlers
        var elementTimelineControl  = query('#timeline-control'),
            elementThresholdControl = query('input[name=\"threshold\"]');

        window.onresize                 = canvasAutoUpdateOnResizeAndSubmit;
        elementTimelineControl.onsubmit = canvasAutoUpdateOnResizeAndSubmit;

        elementThresholdControl.onclick  = canvasAutoUpdateOnThresholdChange;
        elementThresholdControl.onchange = canvasAutoUpdateOnThresholdChange;
        elementThresholdControl.onkeyup  = canvasAutoUpdateOnThresholdChange;

        window.setTimeout(function() {
            canvasAutoUpdateOnThresholdChange(null);
        }, 50);

    //]]>";
        // line 446
        echo "</script>
";
    }

    // line 449
    public function getdump_request_data($_token = null, $_profile = null, $_events = null, $_origin = null)
    {
        $context = $this->env->mergeGlobals(array(
            "token" => $_token,
            "profile" => $_profile,
            "events" => $_events,
            "origin" => $_origin,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 451
            $context["__internal_506f154a0caf8737f5635de43986e9cfd36391c71f79e1e42d0cb22eb10a3830"] = $this;
            // line 452
            echo "                {
                    \"id\": \"";
            // line 453
            echo twig_escape_filter($this->env, $this->getContext($context, "token"), "js", null, true);
            echo "\",
                    \"left\": ";
            // line 454
            echo twig_escape_filter($this->env, sprintf("%F", ($this->getAttribute($this->getAttribute($this->getContext($context, "events"), "__section__"), "origin") - $this->getContext($context, "origin"))), "js", null, true);
            echo ",
                    \"events\": [
";
            // line 456
            echo $context["__internal_506f154a0caf8737f5635de43986e9cfd36391c71f79e1e42d0cb22eb10a3830"]->getdump_events($this->getContext($context, "events"));
            echo "
                    ]
                }
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 462
    public function getdump_events($_events = null)
    {
        $context = $this->env->mergeGlobals(array(
            "events" => $_events,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 464
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "events"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["name"] => $context["event"]) {
                // line 465
                if (("__section__" != $this->getContext($context, "name"))) {
                    // line 466
                    echo "                        {
                            \"name\": \"";
                    // line 467
                    echo twig_escape_filter($this->env, $this->getContext($context, "name"), "js", null, true);
                    echo "\",
                            \"category\": \"";
                    // line 468
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "event"), "category"), "js", null, true);
                    echo "\",
                            \"origin\": ";
                    // line 469
                    echo twig_escape_filter($this->env, sprintf("%F", $this->getAttribute($this->getContext($context, "event"), "origin")), "js", null, true);
                    echo ",
                            \"starttime\": ";
                    // line 470
                    echo twig_escape_filter($this->env, sprintf("%F", $this->getAttribute($this->getContext($context, "event"), "starttime")), "js", null, true);
                    echo ",
                            \"endtime\": ";
                    // line 471
                    echo twig_escape_filter($this->env, sprintf("%F", $this->getAttribute($this->getContext($context, "event"), "endtime")), "js", null, true);
                    echo ",
                            \"duration\": ";
                    // line 472
                    echo twig_escape_filter($this->env, sprintf("%F", $this->getAttribute($this->getContext($context, "event"), "duration")), "js", null, true);
                    echo ",
                            \"memory\": ";
                    // line 473
                    echo twig_escape_filter($this->env, sprintf("%.1F", (($this->getAttribute($this->getContext($context, "event"), "memory") / 1024) / 1024)), "js", null, true);
                    echo ",
                            \"periods\": [";
                    // line 475
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "event"), "periods"));
                    $context['loop'] = array(
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["period"]) {
                        // line 476
                        echo "{\"start\": ";
                        echo twig_escape_filter($this->env, sprintf("%F", $this->getAttribute($this->getContext($context, "period"), "starttime")), "js", null, true);
                        echo ", \"end\": ";
                        echo twig_escape_filter($this->env, sprintf("%F", $this->getAttribute($this->getContext($context, "period"), "endtime")), "js", null, true);
                        echo "}";
                        echo (($this->getAttribute($this->getContext($context, "loop"), "last")) ? ("") : (", "));
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['period'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 478
                    echo "]
                        }";
                    // line 479
                    echo (($this->getAttribute($this->getContext($context, "loop"), "last")) ? ("") : (","));
                    echo "
";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['event'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 485
    public function getdisplay_timeline($_id = null, $_events = null, $_colors = null)
    {
        $context = $this->env->mergeGlobals(array(
            "id" => $_id,
            "events" => $_events,
            "colors" => $_colors,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 486
            echo "    <div class=\"sf-profiler-timeline\">
        <div class=\"legends\">
            ";
            // line 488
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "colors"));
            foreach ($context['_seq'] as $context["category"] => $context["color"]) {
                // line 489
                echo "                <span data-color=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "color"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getContext($context, "category"), "html", null, true);
                echo "</span>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['category'], $context['color'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 491
            echo "        </div>
        <canvas width=\"680\" height=\"\" id=\"";
            // line 492
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "\" class=\"timeline\"></canvas>
    </div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  810 => 492,  792 => 488,  788 => 486,  775 => 485,  706 => 473,  702 => 472,  698 => 471,  690 => 469,  686 => 468,  682 => 467,  679 => 466,  677 => 465,  660 => 464,  634 => 456,  629 => 454,  625 => 453,  622 => 452,  620 => 451,  606 => 449,  549 => 411,  532 => 410,  522 => 406,  389 => 160,  386 => 159,  378 => 157,  367 => 155,  358 => 151,  345 => 147,  340 => 145,  302 => 125,  296 => 121,  357 => 123,  327 => 114,  318 => 111,  306 => 107,  291 => 102,  274 => 110,  462 => 202,  449 => 198,  446 => 197,  431 => 189,  429 => 188,  394 => 168,  380 => 158,  373 => 156,  361 => 152,  348 => 140,  325 => 129,  323 => 128,  320 => 127,  315 => 131,  303 => 106,  300 => 105,  270 => 102,  267 => 101,  262 => 98,  172 => 57,  110 => 43,  14 => 1,  23 => 1,  170 => 84,  167 => 76,  113 => 48,  155 => 47,  97 => 52,  100 => 39,  20 => 1,  232 => 88,  213 => 78,  185 => 74,  175 => 58,  118 => 49,  65 => 17,  58 => 25,  34 => 5,  1361 => 391,  1352 => 390,  1331 => 384,  1324 => 383,  1322 => 382,  1319 => 381,  1296 => 377,  1269 => 375,  1266 => 374,  1254 => 369,  1249 => 368,  1247 => 367,  1244 => 366,  1235 => 360,  1229 => 358,  1226 => 357,  1221 => 356,  1216 => 354,  1209 => 349,  1200 => 347,  1196 => 346,  1193 => 345,  1190 => 344,  1188 => 343,  1185 => 342,  1177 => 338,  1160 => 330,  1155 => 328,  1152 => 327,  1143 => 322,  1141 => 321,  1098 => 313,  1095 => 312,  1079 => 306,  1077 => 305,  1074 => 304,  1067 => 299,  1064 => 298,  1056 => 293,  1053 => 292,  1051 => 291,  1048 => 290,  1040 => 285,  1032 => 283,  1029 => 282,  1027 => 281,  1024 => 280,  1016 => 276,  1014 => 272,  1004 => 266,  982 => 261,  979 => 260,  970 => 257,  967 => 256,  964 => 255,  961 => 254,  958 => 253,  952 => 251,  950 => 250,  939 => 243,  936 => 242,  934 => 241,  931 => 240,  923 => 236,  918 => 234,  915 => 233,  903 => 229,  897 => 227,  894 => 226,  892 => 225,  889 => 224,  881 => 220,  878 => 219,  876 => 218,  873 => 217,  865 => 213,  857 => 210,  849 => 206,  846 => 205,  844 => 204,  841 => 203,  833 => 199,  830 => 198,  828 => 197,  825 => 196,  817 => 192,  812 => 190,  809 => 189,  801 => 185,  798 => 184,  796 => 489,  793 => 182,  772 => 172,  769 => 171,  764 => 169,  756 => 165,  753 => 164,  751 => 163,  749 => 479,  746 => 478,  739 => 156,  729 => 155,  724 => 154,  721 => 153,  715 => 151,  710 => 475,  697 => 141,  695 => 139,  689 => 137,  683 => 135,  680 => 134,  678 => 133,  675 => 132,  666 => 126,  649 => 462,  640 => 119,  635 => 117,  619 => 113,  614 => 111,  598 => 107,  596 => 106,  593 => 105,  576 => 101,  564 => 99,  557 => 96,  550 => 94,  547 => 93,  529 => 409,  527 => 408,  524 => 90,  503 => 81,  496 => 79,  493 => 78,  490 => 77,  478 => 74,  470 => 73,  459 => 69,  456 => 68,  450 => 64,  442 => 62,  433 => 60,  426 => 58,  414 => 52,  408 => 176,  405 => 49,  403 => 48,  377 => 37,  366 => 33,  350 => 26,  342 => 137,  332 => 116,  308 => 13,  299 => 8,  293 => 120,  290 => 119,  288 => 118,  281 => 114,  250 => 341,  248 => 97,  245 => 87,  222 => 83,  210 => 77,  202 => 94,  191 => 67,  134 => 39,  124 => 62,  76 => 34,  1871 => 658,  1869 => 657,  1867 => 656,  1864 => 655,  1859 => 652,  1848 => 650,  1843 => 649,  1840 => 648,  1837 => 647,  1828 => 642,  1825 => 641,  1821 => 639,  1815 => 637,  1812 => 636,  1809 => 635,  1806 => 634,  1800 => 632,  1794 => 630,  1791 => 629,  1788 => 626,  1768 => 624,  1765 => 623,  1762 => 622,  1759 => 621,  1756 => 620,  1753 => 619,  1751 => 618,  1748 => 617,  1745 => 616,  1742 => 615,  1739 => 614,  1737 => 613,  1734 => 612,  1730 => 611,  1728 => 610,  1725 => 609,  1718 => 602,  1712 => 600,  1706 => 598,  1704 => 597,  1702 => 596,  1699 => 595,  1692 => 588,  1688 => 586,  1681 => 584,  1678 => 580,  1674 => 579,  1669 => 578,  1666 => 577,  1659 => 575,  1656 => 570,  1651 => 569,  1648 => 568,  1645 => 567,  1642 => 566,  1627 => 564,  1624 => 563,  1620 => 562,  1617 => 561,  1599 => 560,  1597 => 559,  1595 => 558,  1592 => 557,  1584 => 551,  1582 => 550,  1580 => 549,  1578 => 548,  1575 => 547,  1566 => 543,  1561 => 541,  1559 => 540,  1556 => 539,  1548 => 534,  1542 => 532,  1540 => 531,  1520 => 530,  1517 => 529,  1509 => 523,  1506 => 522,  1500 => 520,  1497 => 519,  1494 => 518,  1491 => 517,  1486 => 515,  1483 => 514,  1480 => 513,  1478 => 512,  1475 => 511,  1466 => 506,  1463 => 505,  1460 => 504,  1457 => 503,  1455 => 502,  1452 => 501,  1443 => 497,  1441 => 496,  1438 => 495,  1431 => 489,  1426 => 488,  1424 => 487,  1421 => 486,  1413 => 479,  1407 => 477,  1405 => 476,  1402 => 475,  1398 => 473,  1396 => 472,  1386 => 470,  1380 => 468,  1378 => 467,  1375 => 466,  1372 => 465,  1369 => 464,  1367 => 463,  1364 => 462,  1362 => 461,  1356 => 459,  1350 => 389,  1347 => 388,  1345 => 455,  1342 => 454,  1333 => 450,  1327 => 448,  1323 => 447,  1318 => 446,  1312 => 443,  1309 => 442,  1306 => 441,  1304 => 440,  1301 => 439,  1294 => 436,  1291 => 435,  1283 => 431,  1279 => 430,  1274 => 429,  1271 => 376,  1265 => 424,  1259 => 422,  1256 => 421,  1250 => 419,  1248 => 418,  1240 => 417,  1237 => 416,  1234 => 415,  1228 => 411,  1222 => 409,  1219 => 355,  1213 => 406,  1211 => 405,  1205 => 404,  1202 => 403,  1199 => 402,  1192 => 399,  1189 => 398,  1181 => 392,  1175 => 337,  1172 => 336,  1166 => 332,  1163 => 386,  1157 => 329,  1154 => 383,  1148 => 381,  1146 => 380,  1142 => 379,  1140 => 378,  1124 => 377,  1121 => 376,  1118 => 320,  1115 => 319,  1112 => 318,  1109 => 317,  1106 => 316,  1103 => 315,  1100 => 314,  1097 => 368,  1094 => 367,  1091 => 366,  1088 => 308,  1086 => 364,  1084 => 307,  1081 => 362,  1069 => 358,  1066 => 357,  1063 => 356,  1060 => 355,  1058 => 354,  1055 => 353,  1046 => 346,  1043 => 345,  1036 => 284,  1033 => 341,  1030 => 340,  1028 => 339,  1021 => 338,  1015 => 336,  1012 => 271,  1009 => 270,  1007 => 333,  1005 => 332,  1003 => 331,  1000 => 330,  992 => 326,  989 => 325,  987 => 324,  984 => 323,  976 => 259,  973 => 258,  971 => 317,  968 => 316,  959 => 310,  955 => 252,  951 => 308,  947 => 249,  943 => 306,  938 => 305,  935 => 304,  932 => 303,  926 => 301,  920 => 235,  912 => 297,  900 => 228,  891 => 295,  888 => 294,  885 => 293,  883 => 292,  880 => 291,  871 => 286,  868 => 285,  862 => 212,  860 => 211,  856 => 281,  851 => 280,  848 => 279,  845 => 278,  838 => 275,  832 => 272,  826 => 271,  814 => 191,  807 => 491,  804 => 268,  802 => 267,  800 => 266,  797 => 265,  789 => 260,  785 => 178,  783 => 177,  782 => 256,  781 => 255,  780 => 176,  777 => 253,  774 => 252,  767 => 170,  761 => 246,  755 => 245,  737 => 244,  730 => 243,  727 => 476,  725 => 241,  723 => 240,  720 => 239,  712 => 150,  709 => 233,  707 => 148,  699 => 142,  696 => 140,  694 => 470,  681 => 225,  673 => 223,  671 => 222,  668 => 221,  664 => 219,  662 => 125,  658 => 124,  656 => 215,  654 => 123,  651 => 213,  645 => 209,  643 => 120,  638 => 118,  632 => 206,  628 => 204,  626 => 203,  621 => 201,  617 => 112,  601 => 446,  595 => 197,  592 => 196,  587 => 195,  584 => 194,  581 => 193,  578 => 192,  575 => 191,  572 => 190,  570 => 189,  567 => 414,  558 => 183,  555 => 95,  546 => 180,  541 => 179,  538 => 178,  523 => 176,  520 => 175,  517 => 404,  515 => 85,  512 => 84,  509 => 83,  506 => 170,  504 => 169,  501 => 80,  498 => 163,  495 => 162,  492 => 161,  489 => 160,  486 => 159,  484 => 158,  481 => 157,  467 => 72,  464 => 71,  458 => 147,  447 => 143,  443 => 142,  439 => 195,  432 => 140,  428 => 59,  421 => 137,  418 => 136,  411 => 132,  400 => 47,  395 => 125,  388 => 42,  364 => 112,  359 => 110,  356 => 109,  351 => 120,  346 => 107,  343 => 146,  329 => 131,  316 => 16,  280 => 82,  277 => 2,  271 => 374,  266 => 1,  260 => 363,  257 => 74,  225 => 298,  195 => 65,  253 => 100,  207 => 75,  198 => 52,  190 => 76,  104 => 32,  338 => 135,  335 => 134,  333 => 135,  326 => 138,  321 => 135,  311 => 14,  304 => 118,  301 => 89,  297 => 104,  286 => 112,  275 => 105,  265 => 105,  261 => 104,  255 => 101,  237 => 65,  233 => 87,  216 => 79,  211 => 41,  206 => 55,  200 => 72,  188 => 90,  186 => 239,  180 => 56,  174 => 65,  153 => 77,  150 => 55,  129 => 64,  126 => 39,  81 => 23,  70 => 15,  53 => 12,  445 => 126,  441 => 196,  438 => 114,  424 => 114,  422 => 184,  419 => 109,  415 => 180,  412 => 106,  406 => 130,  401 => 172,  390 => 43,  385 => 41,  382 => 92,  376 => 90,  371 => 156,  363 => 153,  353 => 149,  349 => 80,  347 => 77,  344 => 119,  336 => 87,  334 => 141,  331 => 140,  328 => 139,  324 => 113,  317 => 67,  313 => 15,  310 => 92,  307 => 128,  295 => 87,  292 => 86,  289 => 113,  279 => 54,  276 => 111,  263 => 95,  259 => 103,  256 => 96,  251 => 99,  249 => 96,  244 => 50,  239 => 49,  236 => 48,  234 => 64,  231 => 83,  226 => 84,  223 => 80,  218 => 59,  215 => 280,  212 => 78,  197 => 69,  194 => 68,  192 => 90,  184 => 63,  181 => 65,  178 => 59,  165 => 83,  161 => 63,  152 => 46,  148 => 69,  146 => 50,  137 => 65,  127 => 35,  114 => 44,  90 => 42,  84 => 40,  77 => 21,  480 => 75,  474 => 153,  469 => 158,  461 => 70,  457 => 153,  453 => 199,  444 => 149,  440 => 148,  437 => 61,  435 => 146,  430 => 144,  427 => 116,  423 => 57,  413 => 134,  409 => 131,  407 => 131,  402 => 130,  398 => 100,  393 => 124,  387 => 164,  384 => 121,  381 => 119,  379 => 118,  374 => 36,  368 => 34,  365 => 111,  362 => 111,  360 => 77,  355 => 150,  341 => 118,  337 => 22,  322 => 96,  314 => 122,  312 => 130,  309 => 129,  305 => 95,  298 => 120,  294 => 124,  285 => 3,  283 => 115,  278 => 98,  268 => 373,  264 => 84,  258 => 94,  252 => 72,  247 => 78,  241 => 93,  229 => 87,  220 => 81,  214 => 42,  177 => 54,  169 => 210,  140 => 58,  132 => 65,  128 => 60,  107 => 52,  61 => 12,  273 => 380,  269 => 107,  254 => 100,  243 => 92,  240 => 326,  238 => 312,  235 => 89,  230 => 303,  227 => 86,  224 => 81,  221 => 79,  219 => 78,  217 => 77,  208 => 76,  204 => 267,  179 => 85,  159 => 196,  143 => 51,  135 => 13,  119 => 40,  102 => 24,  71 => 10,  67 => 14,  63 => 18,  59 => 16,  38 => 18,  94 => 21,  89 => 28,  85 => 24,  75 => 19,  68 => 30,  56 => 35,  87 => 41,  21 => 1,  26 => 11,  93 => 31,  88 => 25,  78 => 21,  46 => 13,  27 => 3,  44 => 20,  31 => 4,  28 => 3,  201 => 53,  196 => 92,  183 => 58,  171 => 81,  166 => 54,  163 => 82,  158 => 80,  156 => 62,  151 => 59,  142 => 59,  138 => 69,  136 => 71,  121 => 50,  117 => 39,  105 => 25,  91 => 33,  62 => 27,  49 => 14,  24 => 18,  25 => 3,  19 => 1,  79 => 18,  72 => 18,  69 => 17,  47 => 21,  40 => 6,  37 => 5,  22 => 17,  246 => 136,  157 => 33,  145 => 74,  139 => 49,  131 => 45,  123 => 61,  120 => 31,  115 => 16,  111 => 47,  108 => 33,  101 => 31,  98 => 45,  96 => 37,  83 => 33,  74 => 20,  66 => 18,  55 => 24,  52 => 12,  50 => 22,  43 => 12,  41 => 19,  35 => 6,  32 => 6,  29 => 5,  209 => 72,  203 => 73,  199 => 93,  193 => 64,  189 => 66,  187 => 75,  182 => 87,  176 => 86,  173 => 85,  168 => 61,  164 => 46,  162 => 59,  154 => 60,  149 => 51,  147 => 75,  144 => 42,  141 => 73,  133 => 55,  130 => 46,  125 => 42,  122 => 41,  116 => 57,  112 => 36,  109 => 52,  106 => 51,  103 => 34,  99 => 23,  95 => 22,  92 => 43,  86 => 45,  82 => 19,  80 => 27,  73 => 33,  64 => 23,  60 => 14,  57 => 14,  54 => 13,  51 => 13,  48 => 11,  45 => 9,  42 => 7,  39 => 6,  36 => 5,  33 => 4,  30 => 5,);
    }
}
