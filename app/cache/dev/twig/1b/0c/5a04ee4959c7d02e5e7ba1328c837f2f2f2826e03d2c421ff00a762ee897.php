<?php

/* FrontBundle:Login:index.html.twig */
class __TwigTemplate_1b0c5a04ee4959c7d02e5e7ba1328c837f2f2f2826e03d2c421ff00a762ee897 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Bienvenido";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/css/bootstrap.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/login/login.css"), "html", null, true);
        echo "\" />

";
    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "
    ";
        // line 14
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 15
            echo "        <div>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "message"), "html", null, true);
            echo "</div>
    ";
        }
        // line 17
        echo "
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-6 col-md-4 col-md-offset-4\">
                    <h1 class=\"text-center login-title\">Bienvenido a ApiShow</h1>
                    <div class=\"account-wall\">
                        <img class=\"profile-img\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/login/logo.png"), "html", null, true);
        echo "\">
                        <form class=\"form-signin\"
                              action=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("front_login_check");
        echo "\" method=\"post\">
                        <input id=\"username\" type=\"text\" name=\"_username\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\"
                               class=\"form-control\" placeholder=\"Usuario\" required autofocus>
                        <input id=\"password\" type=\"password\" name=\"_password\"
                               class=\"form-control\" placeholder=\"Contraseña\" required>
                        <button class=\"btn btn-md btn-primary btn-block\" name=\"login\" type=\"submit\">
                            Entrar</button>
                        </form>
                     </div>
                </div>
           </div>
       </div>
   
";
    }

    public function getTemplateName()
    {
        return "FrontBundle:Login:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 26,  78 => 25,  73 => 23,  65 => 17,  59 => 15,  57 => 14,  54 => 13,  51 => 12,  44 => 7,  39 => 6,  36 => 5,  30 => 3,);
    }
}
