<?php

/* ApiBundle:Default:index.html.twig */
class __TwigTemplate_33a55324a7e0a4c1c67bf3aab3638366835a0b91df136de69b30f351ca035731 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Bienvenido";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/api/bootstrap.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/api/style.css"), "html", null, true);
        echo "\" />

";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo " 
<div class=\"navbar\">
      <div class=\"navbar-inner\">
        <div class=\"container\"> <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
            <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> 
            <a class=\"brand\" href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("api_homepage");
        echo "\"><img class=\"profile-img\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/api/usuario.png"), "html", null, true);
        echo "\"></a>
          <ul class=\"nav nav-collapse pull-right\">
                <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("api_homepage");
        echo "\" class=\"active\">Novedades</a></li>
                <li><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("api_peliculas");
        echo "\">Peliculas</a></li>
                <li><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("api_series");
        echo "\">Series</a></li>
                <li><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("front_logout");
        echo "\">Cerrar Sesión</a></li>
          </ul>
        </div>
      </div>
</div>

<div class=\"container novedades \">
    <div class=\"row-fluid\">
         <div class=\"span4\">
             <div class=\"titulo\">";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["titulo0"]) ? $context["titulo0"] : $this->getContext($context, "titulo0")), "html", null, true);
        echo "</div>
             <div><img src=\"";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["imagen0"]) ? $context["imagen0"] : $this->getContext($context, "imagen0")), "html", null, true);
        echo "\"></div>
         </div>
         <div class=\"span4\">
             <div class=\"titulo\">";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["titulo4"]) ? $context["titulo4"] : $this->getContext($context, "titulo4")), "html", null, true);
        echo "</div>
             <div><img src=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["imagen4"]) ? $context["imagen4"] : $this->getContext($context, "imagen4")), "html", null, true);
        echo "\"></div>
         </div>
         <div class=\"span4\"> 
             <div class=\"titulo\">";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["titulo2"]) ? $context["titulo2"] : $this->getContext($context, "titulo2")), "html", null, true);
        echo "</div>
             <div><img src=\"";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["imagen2"]) ? $context["imagen2"] : $this->getContext($context, "imagen2")), "html", null, true);
        echo "\"></div>
         </div>
    </div>
    
    <div class=\"row-fluid\">
         <div class=\"span4\">
             <div class=\"titulo\">";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["titulo3"]) ? $context["titulo3"] : $this->getContext($context, "titulo3")), "html", null, true);
        echo "</div>
             <div><img src=\"";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["imagen3"]) ? $context["imagen3"] : $this->getContext($context, "imagen3")), "html", null, true);
        echo "\"></div>
         </div>
         <div class=\"span4\"><div class=\"titulo\">";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["titulo5"]) ? $context["titulo5"] : $this->getContext($context, "titulo5")), "html", null, true);
        echo "</div>
         <div><img src=\"";
        // line 50
        echo twig_escape_filter($this->env, (isset($context["imagen5"]) ? $context["imagen5"] : $this->getContext($context, "imagen5")), "html", null, true);
        echo "\"></div> 
         </div>
         <div class=\"span4\"> <div class=\"titulo\">";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["titulo6"]) ? $context["titulo6"] : $this->getContext($context, "titulo6")), "html", null, true);
        echo "</div>
         <div><img src=\"";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["imagen6"]) ? $context["imagen6"] : $this->getContext($context, "imagen6")), "html", null, true);
        echo "\"></div>
         </div>
    </div>

</div>       
            


";
    }

    // line 63
    public function block_javascripts($context, array $blocks = array())
    {
        // line 64
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/js/jquery.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/js/bootstrap.js"), "html", null, true);
        echo "\"></script>

";
    }

    public function getTemplateName()
    {
        return "ApiBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 65,  164 => 64,  161 => 63,  148 => 53,  144 => 52,  139 => 50,  135 => 49,  130 => 47,  126 => 46,  117 => 40,  113 => 39,  107 => 36,  103 => 35,  97 => 32,  93 => 31,  81 => 22,  77 => 21,  73 => 20,  69 => 19,  62 => 17,  55 => 12,  52 => 11,  45 => 7,  40 => 6,  37 => 5,  31 => 3,);
    }
}
