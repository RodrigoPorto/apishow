<?php

/* AdminBundle:User:show.html.twig */
class __TwigTemplate_9f51823044901b8f21e8ecc97d1afbe0261740c49cbe1c8850081cc768fe9090 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Mostrar";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/admin/adminpage/bootstrap.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/apishow/admin/adminpage/main.css"), "html", null, true);
        echo "\" />

";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "<div class=\"navbar navbar-inverse navbar-static-top\">
          <div class=\"container\">
             <a class=\"navbar-brand\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("admin_logout");
        echo "\">Salir</a>
          </div>
   </div>

   <div class=\"container\">
        <div class=\"row\">
             <div class=\"col-lg-8 col-lg-offset-2 centered\">
                   <h1>Usuario</h1>
                   <table class=\"table table-striped custab centered\">
                       <tbody>
                           <tr>
                           <th>Id</th>
                           <td>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
        echo "</td>
                           </tr>
                           <tr>
                           <th>Nombre</th>
                           <td>";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "username"), "html", null, true);
        echo "</td>
                           </tr>
                           <tr>
                           <th>Contraseña</th>
                           <td>";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "password"), "html", null, true);
        echo "</td>
                           </tr>
                           <tr>
                           <th>Activo</th>
                           <td>";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "isActive"), "html", null, true);
        echo "</td>
                           </tr>
                           <tr>
                           <th>Editar</th>
                           <td> <a href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
        echo "\"
                                   class=\"btn btn-danger btn-xs\">Editar</a></td>
                           </tr>
                           <tr>
                           <th>Borrar</th>
                           <td>";
        // line 48
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</td>
                           </tr>
                           </tbody>
                           </table>
                   
                            <a href=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("user");
        echo "\">Volver</a>
                  </div>
          </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "AdminBundle:User:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 53,  109 => 48,  101 => 43,  94 => 39,  87 => 35,  80 => 31,  73 => 27,  58 => 15,  54 => 13,  51 => 11,  44 => 7,  39 => 6,  36 => 5,  30 => 3,);
    }
}
