<?php

/* MopaBootstrapBundle:Form:fields.html.twig */
class __TwigTemplate_295dd8973b4907c6003b8b7f90d729a4c04d0119537997cfa0893a66be40a2e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("form_div_layout.html.twig");

        $this->blocks = array(
            'button_attributes' => array($this, 'block_button_attributes'),
            'button_widget' => array($this, 'block_button_widget'),
            'button_row' => array($this, 'block_button_row'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'form_widget_compound' => array($this, 'block_form_widget_compound'),
            'form_tabs' => array($this, 'block_form_tabs'),
            'tabs_widget' => array($this, 'block_tabs_widget'),
            'form_tab' => array($this, 'block_form_tab'),
            'collection_widget' => array($this, 'block_collection_widget'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'file_widget' => array($this, 'block_file_widget'),
            'form_legend' => array($this, 'block_form_legend'),
            'form_label' => array($this, 'block_form_label'),
            'help_label' => array($this, 'block_help_label'),
            'help_label_tooltip' => array($this, 'block_help_label_tooltip'),
            'help_label_popover' => array($this, 'block_help_label_popover'),
            'form_actions_widget' => array($this, 'block_form_actions_widget'),
            'form_actions_row' => array($this, 'block_form_actions_row'),
            'form_rows_visible' => array($this, 'block_form_rows_visible'),
            'form_row' => array($this, 'block_form_row'),
            'form_message' => array($this, 'block_form_message'),
            'form_help' => array($this, 'block_form_help'),
            'form_widget_add_btn' => array($this, 'block_form_widget_add_btn'),
            'form_widget_remove_btn' => array($this, 'block_form_widget_remove_btn'),
            'collection_button' => array($this, 'block_collection_button'),
            'label_asterisk' => array($this, 'block_label_asterisk'),
            'widget_addon' => array($this, 'block_widget_addon'),
            'form_errors' => array($this, 'block_form_errors'),
            'error_type' => array($this, 'block_error_type'),
            'widget_form_group_start' => array($this, 'block_widget_form_group_start'),
            'help_widget_popover' => array($this, 'block_help_widget_popover'),
            'widget_form_group_end' => array($this, 'block_widget_form_group_end'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_button_attributes($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => ("btn " . (($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")))));
        // line 6
        echo "    ";
        $this->displayParentBlock("button_attributes", $context, $blocks);
        echo "
";
    }

    // line 9
    public function block_button_widget($context, array $blocks = array())
    {
        // line 10
        ob_start();
        // line 11
        echo "    ";
        if (twig_test_empty($this->getContext($context, "label"))) {
            // line 12
            echo "        ";
            $context["label"] = call_user_func_array($this->env->getFilter('humanize')->getCallable(), array($this->getContext($context, "name")));
            // line 13
            echo "    ";
        }
        // line 14
        echo "    <button type=\"";
        echo twig_escape_filter($this->env, ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "button")) : ("button")), "html", null, true);
        echo "\" ";
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">
    ";
        // line 15
        if ((!twig_test_empty($this->getContext($context, "icon")))) {
            // line 16
            echo "            ";
            echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getContext($context, "icon"), ((array_key_exists("icon_inverted", $context)) ? (_twig_default_filter($this->getContext($context, "icon_inverted"), false)) : (false)));
            echo "
    ";
        }
        // line 18
        echo "    ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "</button>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 22
    public function block_button_row($context, array $blocks = array())
    {
        // line 23
        echo "    ";
        ob_start();
        // line 24
        echo "        ";
        if ((!twig_test_empty($this->getContext($context, "button_offset")))) {
            // line 25
            echo "            ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("for" => $this->getContext($context, "id"), "class" => $this->getContext($context, "button_offset")));
            // line 26
            echo "            <div class=\"form-group\">
                <div ";
            // line 27
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "attr"));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
                ";
            // line 28
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
            echo "
                </div>
            </div>
        ";
        } else {
            // line 32
            echo "            <div>
                ";
            // line 33
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
            echo "
            </div>
        ";
        }
        // line 36
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 41
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        // line 42
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => (((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " ") . $this->getContext($context, "widget_form_control_class"))));
        // line 43
        echo "    ";
        $this->displayParentBlock("choice_widget_collapsed", $context, $blocks);
        echo "
    ";
        // line 44
        if ($this->getContext($context, "horizontal")) {
            // line 45
            echo "        ";
            $this->displayBlock("form_message", $context, $blocks);
            echo "
    ";
        }
    }

    // line 49
    public function block_textarea_widget($context, array $blocks = array())
    {
        // line 50
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "text")) : ("text"));
        // line 51
        echo "    ";
        if ((($this->getContext($context, "type") != "hidden") && ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_prepend"), null)) : (null)))) || (!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_append"), null)) : (null))))))) {
            // line 52
            echo "    <div class=\"input-group\">
        ";
            // line 53
            if ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_prepend"), null)) : (null))))) {
                // line 54
                echo "            ";
                $context["widget_addon"] = $this->getContext($context, "widget_addon_prepend");
                // line 55
                echo "            ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 57
            echo "    ";
        }
        // line 58
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => (((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " ") . $this->getContext($context, "widget_form_control_class"))));
        // line 59
        echo "    ";
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        echo "
    ";
        // line 60
        if ($this->getContext($context, "horizontal")) {
            // line 61
            echo "            ";
            $this->displayBlock("form_message", $context, $blocks);
            echo "
    ";
        }
        // line 63
        echo "    ";
        if ((($this->getContext($context, "type") != "hidden") && ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_prepend"), null)) : (null)))) || (!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_append"), null)) : (null))))))) {
            // line 64
            echo "        ";
            if ((!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_append"), null)) : (null))))) {
                // line 65
                echo "        ";
                $context["widget_addon"] = $this->getContext($context, "widget_addon_append");
                // line 66
                echo "        ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 68
            echo "    </div>
    ";
        }
    }

    // line 72
    public function block_form_widget_simple($context, array $blocks = array())
    {
        // line 73
        ob_start();
        // line 74
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "text")) : ("text"));
        // line 75
        echo "    ";
        if ((($this->getContext($context, "type") != "hidden") && ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_prepend"), null)) : (null)))) || (!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_append"), null)) : (null))))))) {
            // line 76
            echo "    <div class=\"input-group\">
        ";
            // line 77
            if ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_prepend"), null)) : (null))))) {
                // line 78
                echo "            ";
                $context["widget_addon"] = $this->getContext($context, "widget_addon_prepend");
                // line 79
                echo "            ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 81
            echo "    ";
        }
        // line 82
        echo "    ";
        if ((!((array_key_exists("widget_remove_btn", $context)) ? (_twig_default_filter($this->getContext($context, "widget_remove_btn"), null)) : (null)))) {
            // line 83
            echo "        ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => ((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " not-removable")));
            // line 84
            echo "    ";
        }
        // line 85
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => (((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " ") . $this->getContext($context, "widget_form_control_class"))));
        // line 86
        echo "    ";
        if (($this->getContext($context, "static_text") === true)) {
            // line 87
            echo "        <p class=\"form-control-static\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
            echo "</p>
    ";
        } else {
            // line 89
            echo "        ";
            $this->displayParentBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        }
        // line 91
        echo "    ";
        if ((($this->getContext($context, "type") != "hidden") && ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_prepend"), null)) : (null)))) || (!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_append"), null)) : (null))))))) {
            // line 92
            echo "        ";
            if ((!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_append"), null)) : (null))))) {
                // line 93
                echo "        ";
                $context["widget_addon"] = $this->getContext($context, "widget_addon_append");
                // line 94
                echo "        ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 96
            echo "    </div>
    ";
        }
        // line 98
        echo "    ";
        if ((($this->getContext($context, "type") != "hidden") && $this->getContext($context, "horizontal"))) {
            // line 99
            echo "        ";
            $this->displayBlock("form_message", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 104
    public function block_form_widget_compound($context, array $blocks = array())
    {
        // line 105
        ob_start();
        // line 106
        echo "    ";
        if (($this->getAttribute($this->getContext($context, "form"), "parent") == null)) {
            // line 107
            echo "        ";
            if ($this->getContext($context, "render_fieldset")) {
                echo "<fieldset>";
            }
            // line 108
            echo "        ";
            if ($this->getContext($context, "show_legend")) {
                $this->displayBlock("form_legend", $context, $blocks);
            }
            // line 109
            echo "    ";
        }
        // line 110
        echo "
    ";
        // line 111
        if ($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "tabbed")) {
            // line 112
            echo "        ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'tabs');
            echo "
        <div class=\"tab-content\">
    ";
        }
        // line 115
        echo "
    ";
        // line 116
        $this->displayBlock("form_rows_visible", $context, $blocks);
        echo "

    ";
        // line 118
        if ($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "tabbed")) {
            // line 119
            echo "        </div>
    ";
        }
        // line 121
        echo "
    ";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'rest');
        echo "

    ";
        // line 124
        if (($this->getAttribute($this->getContext($context, "form"), "parent") == null)) {
            // line 125
            echo "        ";
            if ($this->getContext($context, "render_fieldset")) {
                echo "</fieldset>";
            }
            // line 126
            echo "    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 130
    public function block_form_tabs($context, array $blocks = array())
    {
        // line 131
        if ($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "vars", array(), "any", false, true), "tabsView", array(), "any", true, true)) {
            // line 132
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "tabsView"), 'widget');
            echo "
";
        }
    }

    // line 136
    public function block_tabs_widget($context, array $blocks = array())
    {
        // line 137
        ob_start();
        // line 138
        echo "<ul class=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "attr"), "class"), "html", null, true);
        echo "\">
    ";
        // line 139
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "tabs"));
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 140
            echo "        <li";
            if ($this->getAttribute($this->getContext($context, "tab"), "active")) {
                echo " class=\"active\"";
            }
            echo ">
            <a data-toggle=\"tab\" href=\"#";
            // line 141
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "tab"), "id"), "html", null, true);
            echo "\">
                ";
            // line 142
            if ($this->getAttribute($this->getContext($context, "tab"), "icon")) {
                echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getAttribute($this->getContext($context, "tab"), "icon"));
            }
            // line 143
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "tab"), "label"), array(), $this->getAttribute($this->getContext($context, "tab"), "translation_domain")), "html", null, true);
            echo "
            </a>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "</ul>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 151
    public function block_form_tab($context, array $blocks = array())
    {
        // line 152
        echo "<div class=\"tab-pane";
        echo (($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "tab_active")) ? (" active") : (""));
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
        echo "\">
    ";
        // line 153
        $this->displayBlock("form_widget", $context, $blocks);
        echo "
</div>
";
    }

    // line 157
    public function block_collection_widget($context, array $blocks = array())
    {
        // line 158
        ob_start();
        // line 159
        echo "    ";
        if (array_key_exists("prototype", $context)) {
            // line 160
            echo "        ";
            $context["prototype_markup"] = $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "prototype"), 'row');
            // line 161
            echo "        ";
            $context["data_prototype_name"] = (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "name", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "name"), "__name__")) : ("__name__"));
            // line 162
            echo "        ";
            $context["data_prototype_label"] = (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "label", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "label"), "__name__label__")) : ("__name__label__"));
            // line 163
            echo "        ";
            $context["widget_form_group_attr"] = twig_array_merge(twig_array_merge($this->getContext($context, "widget_form_group_attr"), array("data-prototype" => $this->getContext($context, "prototype_markup"), "data-prototype-name" => $this->getContext($context, "data_prototype_name"), "data-prototype-label" => $this->getContext($context, "data_prototype_label"))), $this->getContext($context, "attr"));
            // line 168
            echo "    ";
        }
        // line 169
        echo "    ";
        // line 170
        echo "\t";
        if ((twig_in_filter("collection", $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "block_prefixes")) && $this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true))) {
            // line 171
            echo "\t\t";
            $context["widget_form_group_attr"] = twig_array_merge($this->getContext($context, "widget_form_group_attr"), array("class" => (((($this->getAttribute($this->getContext($context, "widget_form_group_attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "widget_form_group_attr", true), "class"), "row")) : ("row")) . " ") . $this->getAttribute($this->getContext($context, "attr"), "class"))));
            // line 172
            echo "\t";
        }
        // line 173
        echo "    ";
        // line 174
        echo "    ";
        $context["widget_form_group_attr"] = twig_array_merge($this->getContext($context, "widget_form_group_attr"), array("id" => (("collection" . $this->getContext($context, "id")) . "_form_group"), "class" => ((($this->getAttribute($this->getContext($context, "widget_form_group_attr"), "class") . " collection-items ") . $this->getContext($context, "id")) . "_form_group")));
        // line 175
        echo "
    <div ";
        // line 176
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "widget_form_group_attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
    ";
        // line 178
        echo "    ";
        if (((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "value")) == 0) && array_key_exists("prototype", $context))) {
            // line 179
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "prototype_names"));
            foreach ($context['_seq'] as $context["_key"] => $context["name"]) {
                // line 180
                echo "            ";
                echo strtr($this->getContext($context, "prototype_markup"), array("__name__" => $this->getContext($context, "name")));
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['name'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 182
            echo "    ";
        }
        // line 183
        echo "    ";
        $this->displayBlock("form_widget", $context, $blocks);
        echo "
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 188
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        // line 189
        ob_start();
        // line 190
        echo "    ";
        $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("class" => (($this->getAttribute($this->getContext($context, "label_attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "label_attr", true), "class"), "")) : (""))));
        // line 191
        echo "    ";
        $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("class" => (($this->getAttribute($this->getContext($context, "label_attr"), "class") . " ") . ((($this->getContext($context, "widget_type") != "")) ? ((((($this->getContext($context, "multiple")) ? ("checkbox") : ("radio")) . "-") . $this->getContext($context, "widget_type"))) : ("")))));
        // line 192
        echo "    ";
        if ($this->getContext($context, "expanded")) {
            // line 193
            echo "        ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => (($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), $this->getContext($context, "horizontal_input_wrapper_class"))) : ($this->getContext($context, "horizontal_input_wrapper_class")))));
            // line 194
            echo "    ";
        }
        // line 195
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 196
            echo "        ";
            if (($this->getContext($context, "widget_type") != "inline")) {
                // line 197
                echo "        <div class=\"";
                echo (($this->getContext($context, "multiple")) ? ("checkbox") : ("radio"));
                echo "\">
        ";
            }
            // line 199
            echo "            <label";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "label_attr"));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
                ";
            // line 200
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'widget', array("horizontal_label_class" => $this->getContext($context, "horizontal_label_class"), "horizontal_input_wrapper_class" => $this->getContext($context, "horizontal_input_wrapper_class"), "attr" => array("class" => (($this->getAttribute($this->getContext($context, "attr", true), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "widget_class"), "")) : ("")))));
            echo "
                ";
            // line 201
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute($this->getContext($context, "child"), "vars"), "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
            echo "
            </label>
        ";
            // line 203
            if (($this->getContext($context, "widget_type") != "inline")) {
                // line 204
                echo "        </div>
        ";
            }
            // line 206
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 207
        echo "    ";
        $this->displayBlock("form_message", $context, $blocks);
        echo "
    ";
        // line 208
        if ($this->getContext($context, "expanded")) {
            // line 209
            echo "    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 213
    public function block_checkbox_widget($context, array $blocks = array())
    {
        // line 214
        ob_start();
        // line 215
        if (((!($this->getContext($context, "label") === false)) && twig_test_empty($this->getContext($context, "label")))) {
            // line 216
            echo "    ";
            $context["label"] = call_user_func_array($this->env->getFilter('humanize')->getCallable(), array($this->getContext($context, "name")));
        }
        // line 218
        if ((($this->getAttribute($this->getContext($context, "form"), "parent") != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "parent"), "vars"), "block_prefixes")))) {
            // line 219
            echo "    <div class=\"checkbox\">
";
        }
        // line 221
        echo "
";
        // line 222
        if (((($this->getAttribute($this->getContext($context, "form"), "parent") != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "parent"), "vars"), "block_prefixes"))) && $this->getContext($context, "label_render"))) {
            // line 223
            echo "    <label ";
            if ((!$this->getContext($context, "horizontal"))) {
                echo "class=\"checkbox-inline\"";
            }
            echo ">
";
        }
        // line 225
        echo "        <input type=\"checkbox\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
            echo "\"";
        }
        if ($this->getContext($context, "checked")) {
            echo " checked=\"checked\"";
        }
        echo "/>
";
        // line 226
        if ((($this->getAttribute($this->getContext($context, "form"), "parent") != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "parent"), "vars"), "block_prefixes")))) {
            // line 227
            echo "    ";
            if (($this->getContext($context, "label_render") && twig_in_filter($this->getContext($context, "widget_checkbox_label"), array(0 => "both", 1 => "widget")))) {
                // line 228
                echo "        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
                echo "
    </label>
    ";
            }
        }
        // line 232
        if ((($this->getAttribute($this->getContext($context, "form"), "parent") != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "parent"), "vars"), "block_prefixes")))) {
            // line 233
            echo "    </div>
    ";
            // line 234
            $this->displayBlock("form_message", $context, $blocks);
            echo "
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 239
    public function block_date_widget($context, array $blocks = array())
    {
        // line 240
        ob_start();
        // line 241
        if (($this->getContext($context, "widget") == "single_text")) {
            // line 242
            echo "    ";
            if (array_key_exists("datepicker", $context)) {
                // line 243
                echo "        <div data-provider=\"datepicker\" class=\"input-group date\" data-date=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
                echo "\" data-link-field=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                echo "\" data-link-format=\"yyyy-mm-dd\">
            <input ";
                // line 244
                if ((!($this->getContext($context, "widget_form_control_class") === false))) {
                    echo "class=\"";
                    echo twig_escape_filter($this->env, $this->getContext($context, "widget_form_control_class"), "html", null, true);
                    echo "\" ";
                }
                echo "type=\"text\" ";
                if ($this->getContext($context, "read_only")) {
                    echo " readonly=\"readonly\"";
                }
                if ($this->getContext($context, "disabled")) {
                    echo " disabled=\"disabled\"";
                }
                if ($this->getContext($context, "required")) {
                    echo " required=\"required\"";
                }
                echo ">
            <input type=\"hidden\" value=\"";
                // line 245
                echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
                echo "\" ";
                $this->displayBlock("widget_attributes", $context, $blocks);
                echo ">
            <span class=\"input-group-addon\">";
                // line 246
                echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon("calendar");
                echo "</span>
        </div>
    ";
            } else {
                // line 249
                echo "        ";
                $this->displayBlock("form_widget_simple", $context, $blocks);
                echo "
    ";
            }
        } else {
            // line 252
            echo "    ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => (($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "inline")) : ("inline"))));
            // line 253
            echo "    \t<div class=\"row\">
        ";
            // line 254
            echo strtr($this->getContext($context, "date_pattern"), array("{{ year }}" =>             // line 255
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "year"), 'widget', array("attr" => array("class" => ((($this->getAttribute($this->getContext($context, "attr", true), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "widget_class"), "")) : ("")) . "")), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter($this->getContext($context, "horizontal_input_wrapper_class"), "col-lg-3")) : ("col-lg-3")))), "{{ month }}" =>             // line 256
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "month"), 'widget', array("attr" => array("class" => ((($this->getAttribute($this->getContext($context, "attr", true), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "widget_class"), "")) : ("")) . "")), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter($this->getContext($context, "horizontal_input_wrapper_class"), "col-lg-3")) : ("col-lg-3")))), "{{ day }}" =>             // line 257
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "day"), 'widget', array("attr" => array("class" => ((($this->getAttribute($this->getContext($context, "attr", true), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "widget_class"), "")) : ("")) . "")), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter($this->getContext($context, "horizontal_input_wrapper_class"), "col-lg-3")) : ("col-lg-3"))))));
            // line 258
            echo "
        </div>
    ";
            // line 260
            $this->displayBlock("form_message", $context, $blocks);
            echo "
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 265
    public function block_time_widget($context, array $blocks = array())
    {
        // line 266
        ob_start();
        // line 267
        if (($this->getContext($context, "widget") == "single_text")) {
            // line 268
            echo "    ";
            if (array_key_exists("timepicker", $context)) {
                // line 269
                echo "        <div data-provider=\"timepicker\" class=\"input-group date\" data-date=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
                echo "\" data-link-field=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                echo "\" data-link-format=\"hh:ii\">
            <input class=\"form-control\" type=\"text\" ";
                // line 270
                if ($this->getContext($context, "read_only")) {
                    echo " readonly=\"readonly\"";
                }
                if ($this->getContext($context, "disabled")) {
                    echo " disabled=\"disabled\"";
                }
                if ($this->getContext($context, "required")) {
                    echo " required=\"required\"";
                }
                echo ">
            <input type=\"hidden\" value=\"";
                // line 271
                echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
                echo "\" ";
                $this->displayBlock("widget_attributes", $context, $blocks);
                echo ">
            <span class=\"input-group-addon\">";
                // line 272
                echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon("time");
                echo "</span>
        </div>
    ";
            } else {
                // line 275
                echo "        ";
                $this->displayBlock("form_widget_simple", $context, $blocks);
                echo "
    ";
            }
        } else {
            // line 278
            echo "    ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => (($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : (""))));
            // line 279
            echo "    ";
            ob_start();
            // line 280
            echo "    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "hour"), 'widget', array("attr" => array("size" => "1"), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter($this->getContext($context, "horizontal_input_wrapper_class"), "col-lg-2")) : ("col-lg-2"))));
            echo "
    ";
            // line 281
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "minute"), 'widget', array("attr" => array("size" => "1"), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter($this->getContext($context, "horizontal_input_wrapper_class"), "col-lg-2")) : ("col-lg-2"))));
            echo "
    ";
            // line 282
            if ($this->getContext($context, "with_seconds")) {
                // line 283
                echo "        :";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "second"), 'widget', array("attr" => array("size" => "1"), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter($this->getContext($context, "horizontal_input_wrapper_class"), "col-lg-2")) : ("col-lg-2"))));
                echo "
    ";
            }
            // line 285
            echo "    ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 286
            echo "    ";
            $this->displayBlock("form_message", $context, $blocks);
            echo "
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 291
    public function block_datetime_widget($context, array $blocks = array())
    {
        // line 292
        ob_start();
        // line 293
        echo "    ";
        if (($this->getContext($context, "widget") == "single_text")) {
            // line 294
            echo "        ";
            if (array_key_exists("datetimepicker", $context)) {
                // line 295
                echo "            <div data-provider=\"datetimepicker\" class=\"input-group date\" data-date=\"";
                if ($this->getContext($context, "value")) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getContext($context, "value"), "Y-m-d H:i"), "html", null, true);
                }
                echo "\" data-link-field=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                echo "\" data-link-format=\"yyyy-mm-dd hh:ii\">
                <input class=\"form-control\" type=\"text\" ";
                // line 296
                if ($this->getContext($context, "read_only")) {
                    echo " readonly=\"readonly\"";
                }
                if ($this->getContext($context, "disabled")) {
                    echo " disabled=\"disabled\"";
                }
                if ($this->getContext($context, "required")) {
                    echo " required=\"required\"";
                }
                echo ">
                <input type=\"hidden\" value=\"";
                // line 297
                if ($this->getContext($context, "value")) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getContext($context, "value"), "Y-m-d H:i"), "html", null, true);
                }
                echo "\" ";
                $this->displayBlock("widget_attributes", $context, $blocks);
                echo ">
                <span class=\"input-group-addon\">";
                // line 298
                echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon("th");
                echo "</span>
            </div>
        ";
            } else {
                // line 301
                echo "            ";
                $this->displayBlock("form_widget_simple", $context, $blocks);
                echo "
        ";
            }
            // line 303
            echo "    ";
        } else {
            // line 304
            echo "            ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => (($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : (""))));
            // line 305
            echo "            <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
                ";
            // line 306
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "date"), 'errors');
            echo "
                ";
            // line 307
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "time"), 'errors');
            echo "
                ";
            // line 308
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "date"), 'widget', array("attr" => array("class" => (($this->getAttribute($this->getContext($context, "attr", true), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "widget_class"), "")) : (""))), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter($this->getContext($context, "horizontal_input_wrapper_class"), "col-lg-3")) : ("col-lg-3"))));
            echo "
                ";
            // line 309
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "time"), 'widget', array("attr" => array("class" => (($this->getAttribute($this->getContext($context, "attr", true), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "widget_class"), "")) : (""))), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter($this->getContext($context, "horizontal_input_wrapper_class"), "col-lg-2")) : ("col-lg-2"))));
            echo "
                ";
            // line 310
            $this->displayBlock("form_message", $context, $blocks);
            echo "
            </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 316
    public function block_percent_widget($context, array $blocks = array())
    {
        // line 317
        ob_start();
        // line 318
        echo "    ";
        $context["widget_addon_append"] = twig_array_merge($this->getContext($context, "widget_addon_append"), array("text" => (($this->getAttribute($this->getContext($context, "widget_addon_append", true), "text", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "widget_addon_append", true), "text"), "%")) : ("%"))));
        // line 319
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 323
    public function block_money_widget($context, array $blocks = array())
    {
        // line 324
        ob_start();
        // line 325
        echo "    ";
        $context["widget_addon_prepend"] = ((((($this->getContext($context, "widget_addon_prepend") != false) || ($this->getContext($context, "widget_addon_prepend") == null)) && ($this->getContext($context, "money_pattern") != "{{ widget }}"))) ? (array("text" => strtr($this->getContext($context, "money_pattern"), array("{{ widget }}" => "")))) : (((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_prepend"), null)) : (null))));
        // line 326
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 330
    public function block_file_widget($context, array $blocks = array())
    {
        // line 331
        ob_start();
        // line 332
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "file")) : ("file"));
        // line 333
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => (((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " ") . $this->getContext($context, "widget_form_control_class"))));
        // line 334
        echo "    ";
        if ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_prepend"), null)) : (null))))) {
            // line 335
            echo "        ";
            $context["widget_addon"] = $this->getContext($context, "widget_addon_prepend");
            // line 336
            echo "        ";
            $this->displayBlock("widget_addon", $context, $blocks);
            echo "
    ";
        }
        // line 338
        echo "<input type=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "type"), "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo "/>
";
        // line 339
        if ((($this->getContext($context, "type") != "hidden") && (!(null === (($this->getAttribute($this->getContext($context, "widget_addon", true), "type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "widget_addon", true), "type"), null)) : (null)))))) {
            // line 340
            echo "    ";
            if ((!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter($this->getContext($context, "widget_addon_append"), null)) : (null))))) {
                // line 341
                echo "        ";
                $context["widget_addon"] = $this->getContext($context, "widget_addon_append");
                // line 342
                echo "        ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
    ";
            }
        }
        // line 345
        echo "    ";
        if ($this->getContext($context, "horizontal")) {
            // line 346
            echo "        ";
            $this->displayBlock("form_message", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 353
    public function block_form_legend($context, array $blocks = array())
    {
        // line 354
        ob_start();
        // line 355
        echo "    ";
        if (twig_test_empty($this->getContext($context, "label"))) {
            // line 356
            echo "        ";
            $context["label"] = call_user_func_array($this->env->getFilter('humanize')->getCallable(), array($this->getContext($context, "name")));
            // line 357
            echo "    ";
        }
        // line 358
        echo "    <";
        echo twig_escape_filter($this->env, $this->getContext($context, "legend_tag"), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "</";
        echo twig_escape_filter($this->env, $this->getContext($context, "legend_tag"), "html", null, true);
        echo ">
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 362
    public function block_form_label($context, array $blocks = array())
    {
        // line 363
        if ((!twig_in_filter("checkbox", $this->getContext($context, "block_prefixes")) || twig_in_filter($this->getContext($context, "widget_checkbox_label"), array(0 => "label", 1 => "both")))) {
            // line 364
            ob_start();
            // line 365
            echo "    ";
            if ((!($this->getContext($context, "label") === false))) {
                // line 366
                echo "        ";
                if (twig_test_empty($this->getContext($context, "label"))) {
                    // line 367
                    echo "            ";
                    $context["label"] = call_user_func_array($this->env->getFilter('humanize')->getCallable(), array($this->getContext($context, "name")));
                    // line 368
                    echo "        ";
                }
                // line 369
                echo "        ";
                if ((!$this->getContext($context, "compound"))) {
                    // line 370
                    echo "            ";
                    $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("for" => $this->getContext($context, "id")));
                    // line 371
                    echo "        ";
                }
                // line 372
                echo "        ";
                $context["label_attr_class"] = "";
                // line 373
                echo "        ";
                if ($this->getContext($context, "horizontal")) {
                    // line 374
                    echo "            ";
                    $context["label_attr_class"] = (("control-label " . $this->getContext($context, "label_attr_class")) . $this->getContext($context, "horizontal_label_class"));
                    // line 375
                    echo "        ";
                }
                // line 376
                echo "        ";
                $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("class" => ((((($this->getAttribute($this->getContext($context, "label_attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "label_attr", true), "class"), "")) : ("")) . " ") . $this->getContext($context, "label_attr_class")) . (($this->getContext($context, "required")) ? (" required") : (" optional")))));
                // line 377
                echo "        <label";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getContext($context, "label_attr"));
                foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
                    echo "\"";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo ">
        ";
                // line 378
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
                // line 379
                $this->displayBlock("label_asterisk", $context, $blocks);
                echo "
        ";
                // line 380
                if (((twig_in_filter("collection", $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "block_prefixes")) && ((array_key_exists("widget_add_btn", $context)) ? (_twig_default_filter($this->getContext($context, "widget_add_btn"), null)) : (null))) && ($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "allow_add") == true))) {
                    // line 381
                    echo "            ";
                    $this->displayBlock("form_widget_add_btn", $context, $blocks);
                    echo "
        ";
                }
                // line 383
                echo "        ";
                if ($this->getContext($context, "help_label")) {
                    // line 384
                    echo "            ";
                    $this->displayBlock("help_label", $context, $blocks);
                    echo "
        ";
                }
                // line 386
                echo "        ";
                if ($this->getAttribute($this->getContext($context, "help_label_tooltip"), "title")) {
                    // line 387
                    echo "            ";
                    $this->displayBlock("help_label_tooltip", $context, $blocks);
                    echo "
        ";
                }
                // line 389
                echo "        ";
                if ($this->getAttribute($this->getContext($context, "help_label_popover"), "title")) {
                    // line 390
                    echo "            ";
                    $this->displayBlock("help_label_popover", $context, $blocks);
                    echo "
        ";
                }
                // line 392
                echo "        </label>
    ";
            }
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        }
    }

    // line 398
    public function block_help_label($context, array $blocks = array())
    {
        // line 399
        echo "    <span class=\"help-block\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "help_label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "</span>
";
    }

    // line 402
    public function block_help_label_tooltip($context, array $blocks = array())
    {
        // line 403
        echo "    <span class=\"help-block\">
        <a href=\"#\" data-toggle=\"tooltip\" data-placement=\"";
        // line 404
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "help_label_tooltip"), "placement"), "html", null, true);
        echo "\" data-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "help_label_tooltip"), "title"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "\">
            ";
        // line 405
        if ((!($this->getAttribute($this->getContext($context, "help_label_tooltip"), "icon") === false))) {
            // line 406
            echo "                ";
            echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getAttribute($this->getContext($context, "help_label_tooltip"), "icon"));
            echo "
            ";
        }
        // line 408
        echo "            ";
        if ((!($this->getAttribute($this->getContext($context, "help_label_tooltip"), "text") === null))) {
            // line 409
            echo "            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "help_label_tooltip"), "text"), "html", null, true);
            echo "
            ";
        }
        // line 411
        echo "        </a>
    </span>
";
    }

    // line 415
    public function block_help_label_popover($context, array $blocks = array())
    {
        // line 416
        echo "    <span class=\"help-block\">
        <a href=\"#\" data-toggle=\"popover\" data-trigger=\"hover\" data-placement=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "help_label_popover"), "placement"), "html", null, true);
        echo "\" data-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "help_label_popover"), "title"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "\" data-content=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "help_label_popover"), "content"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "\" >
            ";
        // line 418
        if ((!($this->getAttribute($this->getContext($context, "help_label_popover"), "icon") === false))) {
            // line 419
            echo "                ";
            echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getAttribute($this->getContext($context, "help_label_popover"), "icon"));
            echo "
            ";
        }
        // line 421
        echo "            ";
        if ((!($this->getAttribute($this->getContext($context, "help_label_popover"), "text") === null))) {
            // line 422
            echo "            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "help_label_popover"), "text"), "html", null, true);
            echo "
            ";
        }
        // line 424
        echo "        </a>
    </span>
";
    }

    // line 428
    public function block_form_actions_widget($context, array $blocks = array())
    {
        // line 429
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "buttons"));
        foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
            // line 430
            echo "        ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "button"), 'widget');
            echo "&nbsp; ";
            // line 431
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 435
    public function block_form_actions_row($context, array $blocks = array())
    {
        // line 436
        echo "    ";
        $this->displayBlock("button_row", $context, $blocks);
        echo "
";
    }

    // line 439
    public function block_form_rows_visible($context, array $blocks = array())
    {
        // line 440
        ob_start();
        // line 441
        echo "    ";
        if ($this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors')) {
            // line 442
            echo "        <div class=\"symfony-form-errors\">
            ";
            // line 443
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
            echo "
        </div>
    ";
        }
        // line 446
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 447
            echo "        ";
            if (!twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "child"), "vars"), "block_prefixes"))) {
                echo " ";
                // line 448
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'row');
                echo "
        ";
            }
            // line 450
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 454
    public function block_form_row($context, array $blocks = array())
    {
        // line 455
        ob_start();
        // line 456
        echo "    ";
        if (twig_in_filter("tab", $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "block_prefixes"))) {
            // line 457
            echo "        ";
            $this->displayBlock("form_tab", $context, $blocks);
            echo "
    ";
        } else {
            // line 459
            echo "        ";
            $this->displayBlock("widget_form_group_start", $context, $blocks);
            echo "

\t\t";
            // line 461
            $context["show_horizontal_wrapper"] = ($this->getContext($context, "horizontal") && (!((!(null === $this->getAttribute($this->getContext($context, "form"), "parent"))) && twig_in_filter("collection", $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "parent"), "vars"), "block_prefixes")))));
            // line 462
            echo "
        ";
            // line 463
            if (($this->getContext($context, "horizontal") && (!$this->getContext($context, "label_render")))) {
                // line 464
                echo "            ";
                $context["horizontal_input_wrapper_class"] = (($this->getContext($context, "horizontal_input_wrapper_class") . " ") . $this->getContext($context, "horizontal_label_offset_class"));
                // line 465
                echo "        ";
            }
            // line 466
            echo "
\t\t";
            // line 467
            if ($this->getContext($context, "show_horizontal_wrapper")) {
                // line 468
                echo "        <div class=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "horizontal_input_wrapper_class"), "html", null, true);
                echo "\">
\t\t";
            }
            // line 470
            echo "        ";
            echo $this->env->getExtension('translator')->trans($this->getContext($context, "widget_prefix"), array(), $this->getContext($context, "translation_domain"));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget', $context);
            echo " ";
            echo $this->env->getExtension('translator')->trans($this->getContext($context, "widget_suffix"), array(), $this->getContext($context, "translation_domain"));
            echo "

\t\t";
            // line 472
            if ($this->getContext($context, "show_horizontal_wrapper")) {
                // line 473
                echo "        </div>
        ";
            }
            // line 475
            echo "
        ";
            // line 476
            if (((((!(null === $this->getAttribute($this->getContext($context, "form"), "parent"))) && twig_in_filter("collection", $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "parent"), "vars"), "block_prefixes"))) && ((array_key_exists("widget_remove_btn", $context)) ? (_twig_default_filter($this->getContext($context, "widget_remove_btn"), null)) : (null))) && (($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "parent", array(), "any", false, true), "vars", array(), "any", false, true), "allow_delete", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "parent", array(), "any", false, true), "vars", array(), "any", false, true), "allow_delete"), false)) : (false)))) {
                // line 477
                echo "            ";
                $this->displayBlock("form_widget_remove_btn", $context, $blocks);
                echo "
        ";
            }
            // line 479
            $this->displayBlock("widget_form_group_end", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 486
    public function block_form_message($context, array $blocks = array())
    {
        // line 487
        ob_start();
        // line 488
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
        echo "
    ";
        // line 489
        $this->displayBlock("form_help", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 495
    public function block_form_help($context, array $blocks = array())
    {
        // line 496
        ob_start();
        // line 497
        if ($this->getContext($context, "help_block")) {
            echo "<p class=\"help-block\">";
            echo $this->env->getExtension('translator')->trans($this->getContext($context, "help_block"), array(), $this->getContext($context, "translation_domain"));
            echo "</p>";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 501
    public function block_form_widget_add_btn($context, array $blocks = array())
    {
        // line 502
        ob_start();
        // line 503
        echo "    ";
        if (((array_key_exists("widget_add_btn", $context)) ? (_twig_default_filter($this->getContext($context, "widget_add_btn"), null)) : (null))) {
            // line 504
            echo "        ";
            $context["button_type"] = "add";
            // line 505
            echo "        ";
            $context["button_values"] = $this->getContext($context, "widget_add_btn");
            // line 506
            echo "        ";
            $this->displayBlock("collection_button", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 511
    public function block_form_widget_remove_btn($context, array $blocks = array())
    {
        // line 512
        ob_start();
        // line 513
        echo "    ";
        if ((!($this->getAttribute($this->getContext($context, "widget_remove_btn"), "wrapper_div") === false))) {
            // line 514
            echo "        <div class=\"form-group\">
            <div class=\"";
            // line 515
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "widget_remove_btn"), "wrapper_div"), "class"), "html", null, true);
            echo " col-lg-offset-3\">
    ";
        }
        // line 517
        echo "    ";
        if (((array_key_exists("widget_remove_btn", $context)) ? (_twig_default_filter($this->getContext($context, "widget_remove_btn"), null)) : (null))) {
            // line 518
            echo "    ";
            $context["button_type"] = "remove";
            // line 519
            echo "    ";
            $context["button_values"] = $this->getContext($context, "widget_remove_btn");
            // line 520
            echo "    ";
            $this->displayBlock("collection_button", $context, $blocks);
            echo "
    ";
        }
        // line 522
        echo "    ";
        if ((!($this->getAttribute($this->getContext($context, "widget_remove_btn"), "wrapper_div") === false))) {
            // line 523
            echo "            </div>
        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 529
    public function block_collection_button($context, array $blocks = array())
    {
        // line 530
        echo "<a ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "button_values"), "attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " data-collection-";
        echo twig_escape_filter($this->env, $this->getContext($context, "button_type"), "html", null, true);
        echo "-btn=\".";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id", array(), "array"), "html", null, true);
        echo "_form_group\">
";
        // line 531
        if ((!(null === $this->getAttribute($this->getContext($context, "button_values"), "icon")))) {
            // line 532
            echo "    ";
            echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getAttribute($this->getContext($context, "button_values"), "icon"), (($this->getAttribute($this->getContext($context, "button_values", true), "icon_inverted", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "button_values", true), "icon_inverted"), false)) : (false)));
            echo "
";
        }
        // line 534
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "button_values"), "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "
</a>

";
    }

    // line 539
    public function block_label_asterisk($context, array $blocks = array())
    {
        // line 540
        if ($this->getContext($context, "required")) {
            // line 541
            if ($this->getContext($context, "render_required_asterisk")) {
                echo "&nbsp;<span class=\"asterisk\">*</span>";
            }
        } else {
            // line 543
            if ($this->getContext($context, "render_optional_text")) {
                echo "&nbsp;<span>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("(optional)", array(), $this->getContext($context, "translation_domain")), "html", null, true);
                echo "</span>";
            }
        }
    }

    // line 547
    public function block_widget_addon($context, array $blocks = array())
    {
        // line 548
        ob_start();
        // line 549
        $context["widget_addon_icon"] = (($this->getAttribute($this->getContext($context, "widget_addon", true), "icon", array(), "any", true, true)) ? ($this->getAttribute($this->getContext($context, "widget_addon"), "icon")) : (null));
        // line 550
        $context["widget_addon_icon_inverted"] = (($this->getAttribute($this->getContext($context, "widget_addon", true), "icon_inverted", array(), "any", true, true)) ? ($this->getAttribute($this->getContext($context, "widget_addon"), "icon_inverted")) : (false));
        // line 551
        echo "    <span class=\"input-group-addon\">";
        echo (((($this->getAttribute($this->getContext($context, "widget_addon", true), "text", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "widget_addon", true), "text"), false)) : (false))) ? ($this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "widget_addon"), "text"), array(), $this->getContext($context, "translation_domain"))) : ($this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getContext($context, "widget_addon_icon"), $this->getContext($context, "widget_addon_icon_inverted"))));
        echo "</span>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 557
    public function block_form_errors($context, array $blocks = array())
    {
        // line 558
        ob_start();
        // line 559
        if ($this->getContext($context, "error_delay")) {
            // line 560
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 561
                echo "        ";
                if (($this->getAttribute($this->getContext($context, "loop"), "index") == 1)) {
                    // line 562
                    echo "            ";
                    if ($this->getAttribute($this->getContext($context, "child"), "set", array(0 => "errors", 1 => $this->getContext($context, "errors")), "method")) {
                    }
                    // line 563
                    echo "        ";
                }
                // line 564
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 566
            echo "    ";
            if ((twig_length_filter($this->env, $this->getContext($context, "errors")) > 0)) {
                // line 567
                echo "        ";
                if (($this->getAttribute($this->getContext($context, "form"), "parent") == null)) {
                    // line 568
                    echo "            ";
                    $context["__internal_59fc672bb5e3908ae563ba88e2feb34ab8b811ee99d5edbfe7ea3c7f50932d89"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
                    // line 569
                    echo "            ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getContext($context, "errors"));
                    foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                        // line 570
                        echo "                ";
                        echo $context["__internal_59fc672bb5e3908ae563ba88e2feb34ab8b811ee99d5edbfe7ea3c7f50932d89"]->getflash("danger", (((null === $this->getAttribute($this->getContext($context, "error"), "messagePluralization"))) ? ($this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators")) : ($this->env->getExtension('translator')->transchoice($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messagePluralization"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators"))));
                        // line 575
                        echo "
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 577
                    echo "        ";
                } else {
                    // line 578
                    echo "            <span class=\"help-";
                    $this->displayBlock("error_type", $context, $blocks);
                    echo "\">
            ";
                    // line 579
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getContext($context, "errors"));
                    foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                        // line 580
                        echo "                ";
                        echo twig_escape_filter($this->env, (((null === $this->getAttribute($this->getContext($context, "error"), "messagePluralization"))) ? ($this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators")) : ($this->env->getExtension('translator')->transchoice($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messagePluralization"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators"))), "html", null, true);
                        // line 584
                        echo " <br>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 586
                    echo "            </span>
        ";
                }
                // line 588
                echo "    ";
            }
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 595
    public function block_error_type($context, array $blocks = array())
    {
        // line 596
        ob_start();
        // line 597
        if ($this->getContext($context, "error_type")) {
            // line 598
            echo "    ";
            echo twig_escape_filter($this->env, $this->getContext($context, "error_type"), "html", null, true);
            echo "
";
        } elseif (($this->getAttribute($this->getContext($context, "form"), "parent") == null)) {
            // line 600
            echo "    ";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "vars", array(), "any", false, true), "error_type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getContext($context, "form", true), "vars", array(), "any", false, true), "error_type"), "inline")) : ("inline")), "html", null, true);
            echo "
";
        } else {
            // line 602
            echo "    block
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 609
    public function block_widget_form_group_start($context, array $blocks = array())
    {
        // line 610
        if ((((array_key_exists("widget_form_group", $context)) ? (_twig_default_filter($this->getContext($context, "widget_form_group"), false)) : (false)) || ($this->getAttribute($this->getContext($context, "form"), "parent") == null))) {
            // line 611
            echo "    ";
            if (((!(null === $this->getAttribute($this->getContext($context, "form"), "parent"))) && twig_in_filter("collection", $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "parent"), "vars"), "block_prefixes")))) {
                echo " ";
                // line 612
                echo "        ";
                if ((!$this->getContext($context, "omit_collection_item"))) {
                    // line 613
                    echo "            ";
                    // line 614
                    echo "        \t";
                    $context["widget_form_group_attr"] = twig_array_merge($this->getContext($context, "widget_form_group_attr"), array("class" => "collection-item"));
                    // line 615
                    echo "        ";
                }
                // line 616
                echo "    ";
            }
            // line 617
            echo "    ";
            if ((twig_length_filter($this->env, $this->getContext($context, "errors")) > 0)) {
                // line 618
                echo "\t    ";
                // line 619
                echo "\t    ";
                $context["widget_form_group_attr"] = twig_array_merge($this->getContext($context, "widget_form_group_attr"), array("class" => ((($this->getAttribute($this->getContext($context, "widget_form_group_attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "widget_form_group_attr", true), "class"), "")) : ("")) . " has-error")));
                // line 620
                echo "    ";
            }
            // line 621
            echo "    ";
            if (($this->getAttribute($this->getContext($context, "help_widget_popover"), "selector") === null)) {
                // line 622
                echo "        ";
                $context["help_widget_popover"] = twig_array_merge($this->getContext($context, "help_widget_popover"), array("selector" => ("#" . $this->getContext($context, "id"))));
                // line 623
                echo "    ";
            }
            // line 624
            echo "    <div";
            if ((!($this->getAttribute($this->getContext($context, "help_widget_popover"), "title") === null))) {
                $this->displayBlock("help_widget_popover", $context, $blocks);
            }
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "widget_form_group_attr"));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
    ";
            // line 626
            echo "    ";
            if (((((twig_length_filter($this->env, $this->getContext($context, "form")) > 0) && ($this->getAttribute($this->getContext($context, "form"), "parent") != null)) && !twig_in_filter("field", $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "block_prefixes"))) && !twig_in_filter("date", $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "block_prefixes")))) {
                // line 629
                echo "        ";
                if ($this->getContext($context, "show_child_legend")) {
                    // line 630
                    echo "            ";
                    $this->displayBlock("form_legend", $context, $blocks);
                    echo "
        ";
                } elseif ($this->getContext($context, "label_render")) {
                    // line 632
                    echo "            ";
                    echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter($this->getContext($context, "label"), null)) : (null))) ? array() : array("label" => $_label_)));
                    echo "
        ";
                } else {
                    // line 634
                    echo "        ";
                }
                // line 635
                echo "    ";
            } else {
                // line 636
                echo "        ";
                if ($this->getContext($context, "label_render")) {
                    // line 637
                    echo "            ";
                    echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter($this->getContext($context, "label"), null)) : (null))) ? array() : array("label" => $_label_)));
                    echo "
        ";
                }
                // line 639
                echo "    ";
            }
        } else {
            // line 641
            echo "    ";
            if ($this->getContext($context, "label_render")) {
                // line 642
                echo "        ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter($this->getContext($context, "label"), null)) : (null))) ? array() : array("label" => $_label_)));
                echo "
    ";
            }
        }
    }

    // line 647
    public function block_help_widget_popover($context, array $blocks = array())
    {
        // line 648
        echo " ";
        ob_start();
        // line 649
        echo " ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "help_widget_popover"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 650
            echo " data-";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "attrvalue"), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter($this->getContext($context, "domain"), "messages")) : ("messages"))), "html", null, true);
            echo "\"
 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 652
        echo " ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 655
    public function block_widget_form_group_end($context, array $blocks = array())
    {
        // line 656
        ob_start();
        // line 657
        if ((((array_key_exists("widget_form_group", $context)) ? (_twig_default_filter($this->getContext($context, "widget_form_group"), false)) : (false)) || ($this->getAttribute($this->getContext($context, "form"), "parent") == null))) {
            // line 658
            echo "    </div>
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "MopaBootstrapBundle:Form:fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1871 => 658,  1869 => 657,  1867 => 656,  1864 => 655,  1859 => 652,  1848 => 650,  1843 => 649,  1840 => 648,  1837 => 647,  1828 => 642,  1825 => 641,  1821 => 639,  1815 => 637,  1812 => 636,  1809 => 635,  1806 => 634,  1800 => 632,  1794 => 630,  1791 => 629,  1788 => 626,  1768 => 624,  1765 => 623,  1762 => 622,  1759 => 621,  1756 => 620,  1753 => 619,  1751 => 618,  1748 => 617,  1745 => 616,  1742 => 615,  1739 => 614,  1737 => 613,  1734 => 612,  1730 => 611,  1728 => 610,  1725 => 609,  1718 => 602,  1712 => 600,  1706 => 598,  1704 => 597,  1702 => 596,  1699 => 595,  1692 => 588,  1688 => 586,  1681 => 584,  1678 => 580,  1674 => 579,  1669 => 578,  1666 => 577,  1659 => 575,  1656 => 570,  1651 => 569,  1648 => 568,  1645 => 567,  1642 => 566,  1627 => 564,  1624 => 563,  1620 => 562,  1617 => 561,  1599 => 560,  1597 => 559,  1595 => 558,  1592 => 557,  1584 => 551,  1582 => 550,  1580 => 549,  1578 => 548,  1575 => 547,  1566 => 543,  1561 => 541,  1559 => 540,  1556 => 539,  1548 => 534,  1542 => 532,  1540 => 531,  1520 => 530,  1517 => 529,  1509 => 523,  1506 => 522,  1500 => 520,  1497 => 519,  1494 => 518,  1491 => 517,  1486 => 515,  1483 => 514,  1480 => 513,  1478 => 512,  1475 => 511,  1466 => 506,  1463 => 505,  1460 => 504,  1457 => 503,  1455 => 502,  1452 => 501,  1443 => 497,  1441 => 496,  1438 => 495,  1431 => 489,  1426 => 488,  1424 => 487,  1421 => 486,  1413 => 479,  1407 => 477,  1405 => 476,  1402 => 475,  1398 => 473,  1396 => 472,  1386 => 470,  1380 => 468,  1378 => 467,  1375 => 466,  1372 => 465,  1369 => 464,  1367 => 463,  1364 => 462,  1362 => 461,  1356 => 459,  1350 => 457,  1347 => 456,  1345 => 455,  1342 => 454,  1333 => 450,  1327 => 448,  1323 => 447,  1318 => 446,  1312 => 443,  1309 => 442,  1306 => 441,  1304 => 440,  1301 => 439,  1294 => 436,  1291 => 435,  1283 => 431,  1279 => 430,  1274 => 429,  1271 => 428,  1265 => 424,  1259 => 422,  1256 => 421,  1250 => 419,  1248 => 418,  1240 => 417,  1237 => 416,  1234 => 415,  1228 => 411,  1222 => 409,  1219 => 408,  1213 => 406,  1211 => 405,  1205 => 404,  1202 => 403,  1199 => 402,  1192 => 399,  1189 => 398,  1181 => 392,  1175 => 390,  1172 => 389,  1166 => 387,  1163 => 386,  1157 => 384,  1154 => 383,  1148 => 381,  1146 => 380,  1142 => 379,  1140 => 378,  1124 => 377,  1121 => 376,  1118 => 375,  1115 => 374,  1112 => 373,  1109 => 372,  1106 => 371,  1103 => 370,  1100 => 369,  1097 => 368,  1094 => 367,  1091 => 366,  1088 => 365,  1086 => 364,  1084 => 363,  1081 => 362,  1069 => 358,  1066 => 357,  1063 => 356,  1060 => 355,  1058 => 354,  1055 => 353,  1046 => 346,  1043 => 345,  1036 => 342,  1033 => 341,  1030 => 340,  1028 => 339,  1021 => 338,  1015 => 336,  1012 => 335,  1009 => 334,  1007 => 333,  1005 => 332,  1003 => 331,  1000 => 330,  992 => 326,  989 => 325,  987 => 324,  984 => 323,  976 => 319,  973 => 318,  971 => 317,  968 => 316,  959 => 310,  955 => 309,  951 => 308,  947 => 307,  943 => 306,  938 => 305,  935 => 304,  932 => 303,  926 => 301,  920 => 298,  912 => 297,  900 => 296,  891 => 295,  888 => 294,  885 => 293,  883 => 292,  880 => 291,  871 => 286,  868 => 285,  862 => 283,  860 => 282,  856 => 281,  851 => 280,  848 => 279,  845 => 278,  838 => 275,  832 => 272,  826 => 271,  814 => 270,  807 => 269,  804 => 268,  802 => 267,  800 => 266,  797 => 265,  789 => 260,  785 => 258,  783 => 257,  782 => 256,  781 => 255,  780 => 254,  777 => 253,  774 => 252,  767 => 249,  761 => 246,  755 => 245,  737 => 244,  730 => 243,  727 => 242,  725 => 241,  723 => 240,  720 => 239,  712 => 234,  709 => 233,  707 => 232,  699 => 228,  696 => 227,  694 => 226,  681 => 225,  673 => 223,  671 => 222,  668 => 221,  664 => 219,  662 => 218,  658 => 216,  656 => 215,  654 => 214,  651 => 213,  645 => 209,  643 => 208,  638 => 207,  632 => 206,  628 => 204,  626 => 203,  621 => 201,  617 => 200,  601 => 199,  595 => 197,  592 => 196,  587 => 195,  584 => 194,  581 => 193,  578 => 192,  575 => 191,  572 => 190,  570 => 189,  567 => 188,  558 => 183,  555 => 182,  546 => 180,  541 => 179,  538 => 178,  523 => 176,  520 => 175,  517 => 174,  515 => 173,  512 => 172,  509 => 171,  506 => 170,  504 => 169,  501 => 168,  498 => 163,  495 => 162,  492 => 161,  489 => 160,  486 => 159,  484 => 158,  481 => 157,  467 => 152,  464 => 151,  458 => 147,  447 => 143,  443 => 142,  439 => 141,  432 => 140,  428 => 139,  421 => 137,  418 => 136,  411 => 132,  400 => 126,  395 => 125,  388 => 122,  364 => 112,  359 => 110,  356 => 109,  351 => 108,  346 => 107,  343 => 106,  329 => 99,  316 => 94,  280 => 82,  277 => 81,  271 => 79,  266 => 77,  260 => 75,  257 => 74,  225 => 61,  195 => 51,  253 => 52,  207 => 40,  198 => 52,  190 => 34,  104 => 15,  338 => 104,  335 => 137,  333 => 135,  326 => 98,  321 => 125,  311 => 121,  304 => 118,  301 => 89,  297 => 125,  286 => 84,  275 => 112,  265 => 106,  261 => 104,  255 => 73,  237 => 65,  233 => 47,  216 => 21,  211 => 41,  206 => 55,  200 => 94,  188 => 86,  186 => 85,  180 => 83,  174 => 43,  153 => 74,  150 => 73,  129 => 26,  126 => 25,  81 => 39,  70 => 5,  53 => 15,  445 => 126,  441 => 115,  438 => 114,  424 => 114,  422 => 110,  419 => 109,  415 => 107,  412 => 106,  406 => 130,  401 => 101,  390 => 96,  385 => 121,  382 => 92,  376 => 90,  371 => 115,  363 => 78,  353 => 148,  349 => 80,  347 => 77,  344 => 76,  336 => 87,  334 => 75,  331 => 74,  328 => 73,  324 => 126,  317 => 67,  313 => 93,  310 => 92,  307 => 91,  295 => 87,  292 => 86,  289 => 85,  279 => 54,  276 => 53,  263 => 76,  259 => 53,  256 => 103,  251 => 99,  249 => 96,  244 => 50,  239 => 49,  236 => 48,  234 => 64,  231 => 63,  226 => 62,  223 => 60,  218 => 59,  215 => 58,  212 => 57,  197 => 90,  194 => 88,  192 => 50,  184 => 50,  181 => 45,  178 => 46,  165 => 78,  161 => 35,  152 => 26,  148 => 30,  146 => 23,  137 => 14,  127 => 36,  114 => 31,  90 => 5,  84 => 128,  77 => 38,  480 => 162,  474 => 153,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 116,  423 => 138,  413 => 134,  409 => 131,  407 => 131,  402 => 130,  398 => 100,  393 => 124,  387 => 122,  384 => 121,  381 => 119,  379 => 118,  374 => 116,  368 => 82,  365 => 111,  362 => 111,  360 => 77,  355 => 85,  341 => 105,  337 => 103,  322 => 96,  314 => 122,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 124,  285 => 89,  283 => 83,  278 => 113,  268 => 78,  264 => 84,  258 => 81,  252 => 72,  247 => 78,  241 => 91,  229 => 63,  220 => 43,  214 => 42,  177 => 82,  169 => 60,  140 => 22,  132 => 27,  128 => 49,  107 => 19,  61 => 13,  273 => 96,  269 => 94,  254 => 100,  243 => 96,  240 => 66,  238 => 85,  235 => 74,  230 => 46,  227 => 45,  224 => 23,  221 => 56,  219 => 22,  217 => 75,  208 => 68,  204 => 39,  179 => 44,  159 => 76,  143 => 23,  135 => 13,  119 => 42,  102 => 52,  71 => 19,  67 => 4,  63 => 7,  59 => 8,  38 => 3,  94 => 14,  89 => 20,  85 => 11,  75 => 11,  68 => 14,  56 => 4,  87 => 14,  21 => 2,  26 => 6,  93 => 44,  88 => 12,  78 => 21,  46 => 7,  27 => 1,  44 => 12,  31 => 5,  28 => 3,  201 => 53,  196 => 36,  183 => 82,  171 => 42,  166 => 71,  163 => 36,  158 => 28,  156 => 75,  151 => 63,  142 => 59,  138 => 68,  136 => 56,  121 => 33,  117 => 22,  105 => 55,  91 => 13,  62 => 9,  49 => 19,  24 => 4,  25 => 3,  19 => 13,  79 => 125,  72 => 43,  69 => 9,  47 => 9,  40 => 8,  37 => 10,  22 => 32,  246 => 68,  157 => 33,  145 => 46,  139 => 18,  131 => 52,  123 => 24,  120 => 23,  115 => 16,  111 => 37,  108 => 56,  101 => 15,  98 => 9,  96 => 31,  83 => 10,  74 => 45,  66 => 32,  55 => 15,  52 => 2,  50 => 14,  43 => 2,  41 => 4,  35 => 5,  32 => 4,  29 => 3,  209 => 106,  203 => 54,  199 => 67,  193 => 35,  189 => 49,  187 => 51,  182 => 66,  176 => 33,  173 => 43,  168 => 41,  164 => 30,  162 => 77,  154 => 32,  149 => 25,  147 => 28,  144 => 49,  141 => 69,  133 => 55,  130 => 41,  125 => 34,  122 => 43,  116 => 41,  112 => 57,  109 => 18,  106 => 36,  103 => 16,  99 => 48,  95 => 28,  92 => 21,  86 => 28,  82 => 126,  80 => 9,  73 => 6,  64 => 38,  60 => 6,  57 => 5,  54 => 3,  51 => 14,  48 => 13,  45 => 17,  42 => 8,  39 => 7,  36 => 5,  33 => 4,  30 => 7,);
    }
}
