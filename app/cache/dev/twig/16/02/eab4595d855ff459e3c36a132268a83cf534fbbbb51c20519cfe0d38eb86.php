<?php

/* MopaBootstrapBundle::base_initializr.html.twig */
class __TwigTemplate_1602eab4595d855ff459e3c36a132268a83cf534fbbbb51c20519cfe0d38eb86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("MopaBootstrapBundle::base_less.html.twig");

        $this->blocks = array(
            'html_tag' => array($this, 'block_html_tag'),
            'head' => array($this, 'block_head'),
            'dns_prefetch' => array($this, 'block_dns_prefetch'),
            'title' => array($this, 'block_title'),
            'head_style' => array($this, 'block_head_style'),
            'head_scripts' => array($this, 'block_head_scripts'),
            'body_start' => array($this, 'block_body_start'),
            'body' => array($this, 'block_body'),
            'navbar' => array($this, 'block_navbar'),
            'container' => array($this, 'block_container'),
            'foot_script' => array($this, 'block_foot_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MopaBootstrapBundle::base_less.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_html_tag($context, array $blocks = array())
    {
        // line 4
        echo "<!--[if lt IE 7]> <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\" lang=\"en\"> <![endif]-->
<!--[if IE 7]>    <html class=\"no-js lt-ie9 lt-ie8\" lang=\"en\"> <![endif]-->
<!--[if IE 8]>    <html class=\"no-js lt-ie9\" lang=\"en\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\" lang=\"en\"> <!--<![endif]-->

";
    }

    // line 14
    public function block_head($context, array $blocks = array())
    {
        // line 15
        echo "<head>
    ";
        // line 17
        echo "    <meta charset=\"utf-8\" />

    ";
        // line 21
        echo "    ";
        $this->displayBlock('dns_prefetch', $context, $blocks);
        // line 26
        echo "
    ";
        // line 32
        echo "    <!--[if IE]><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" /><![endif]-->

    ";
        // line 36
        echo "    <meta name=\"viewport\" content=\"width=device-width\" />
    <meta name=\"description\" content=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "description", array(), "array"), "html", null, true);
        echo "\" />
    <meta name=\"keywords\" content=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "keywords", array(), "array"), "html", null, true);
        echo "\" />
    <meta name=\"author\" content=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "author_name", array(), "array"), "html", null, true);
        echo "\" />
    ";
        // line 41
        echo "    <link rel=\"author\" href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "author_url", array(), "array"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "author_name", array(), "array"), "html", null, true);
        echo "\" />

    ";
        // line 44
        echo "    <title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    ";
        // line 48
        echo "
    ";
        // line 52
        echo "
    ";
        // line 55
        echo "
    <link rel=\"shortcut icon\" href=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    <link rel=\"apple-touch-icon\" href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("apple-touch-icon.png"), "html", null, true);
        echo "\" />

    ";
        // line 60
        echo "    ";
        if ($this->getAttribute($this->getContext($context, "meta", true), "sitemap", array(), "array", true, true)) {
            // line 61
            echo "    <link rel=\"sitemap\" type=\"application/xml\" title=\"Sitemap\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "sitemap", array(), "array"), "html", null, true);
            echo "\" />
    ";
        }
        // line 63
        echo "
    ";
        // line 65
        echo "    ";
        if ($this->getAttribute($this->getContext($context, "meta", true), "feed_atom", array(), "array", true, true)) {
            // line 66
            echo "    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"Atom\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "feed_atom", array(), "array"), "html", null, true);
            echo "\" />
    ";
        }
        // line 68
        echo "    ";
        if ($this->getAttribute($this->getContext($context, "meta", true), "feed_rss", array(), "array", true, true)) {
            // line 69
            echo "    <link rel=\"alternate\" type=\"application/rss+xml\" title=\"RSS\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "feed_rss", array(), "array"), "html", null, true);
            echo "\" />
    ";
        }
        // line 71
        echo "
    ";
        // line 73
        echo "    ";
        if ($this->getAttribute($this->getContext($context, "meta"), "noindex", array(), "array")) {
            // line 74
            echo "        ";
            $context["meta_robots"] = "noindex,";
            // line 75
            echo "    ";
        } else {
            // line 76
            echo "        ";
            $context["meta_robots"] = "";
            // line 77
            echo "    ";
        }
        // line 78
        echo "    ";
        if ($this->getAttribute($this->getContext($context, "meta"), "nofollow", array(), "array")) {
            // line 79
            echo "        ";
            $context["meta_robots"] = ($this->getContext($context, "meta_robots") . "nofollow");
            // line 80
            echo "    ";
        } else {
            // line 81
            echo "        ";
            $context["meta_robots"] = ($this->getContext($context, "meta_robots") . "follow");
            // line 82
            echo "    ";
        }
        // line 83
        echo "    <meta name=\"robots\" content=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "meta_robots"), "html", null, true);
        echo "\" />

    ";
        // line 85
        if ($this->getAttribute($this->getContext($context, "google", true), "wt", array(), "array", true, true)) {
            // line 86
            echo "    <meta name=\"google-site-verification\" content=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "google"), "wt", array(), "array"), "html", null, true);
            echo "\" />
    ";
        }
        // line 88
        echo "
    ";
        // line 90
        echo "
    ";
        // line 94
        echo "
    ";
        // line 96
        echo "    ";
        $this->displayBlock('head_style', $context, $blocks);
        // line 105
        echo "
    ";
        // line 106
        $this->displayBlock('head_scripts', $context, $blocks);
        // line 109
        echo "</head>
";
    }

    // line 21
    public function block_dns_prefetch($context, array $blocks = array())
    {
        // line 22
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "dns_prefetch"));
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 23
            echo "        <link rel=\"dns-prefetch\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "domain"), "html", null, true);
            echo "\" />
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "    ";
    }

    // line 44
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "meta"), "title", array(), "array"), "html", null, true);
    }

    // line 96
    public function block_head_style($context, array $blocks = array())
    {
        // line 97
        echo "        ";
        $this->displayParentBlock("head_style", $context, $blocks);
        echo "

        ";
        // line 101
        echo "        ";
        if ($this->getContext($context, "diagnostic_mode")) {
            // line 102
            echo "            <link href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/mopabootstrap/css/diagnostic.css"), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" media=\"screen\" />
        ";
        }
        // line 104
        echo "    ";
    }

    // line 106
    public function block_head_scripts($context, array $blocks = array())
    {
        // line 107
        echo "        <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/mopabootstrap/js/modernizr-2.5.3-respond-1.1.0.min.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 112
    public function block_body_start($context, array $blocks = array())
    {
        // line 113
        echo "<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href=\"http://browsehappy.com/\">Upgrade to a different browser</a> or <a href=\"http://www.google.com/chromeframe/?redirect=true\">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
";
    }

    // line 116
    public function block_body($context, array $blocks = array())
    {
        // line 117
        echo "    ";
        $this->displayBlock('navbar', $context, $blocks);
        // line 120
        echo "
    ";
        // line 121
        $this->displayBlock('container', $context, $blocks);
        // line 124
        echo "
    ";
        // line 125
        $this->displayBlock('foot_script', $context, $blocks);
    }

    // line 117
    public function block_navbar($context, array $blocks = array())
    {
        // line 118
        echo "    ";
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
    ";
    }

    // line 121
    public function block_container($context, array $blocks = array())
    {
        // line 122
        echo "    ";
        $this->displayParentBlock("container", $context, $blocks);
        echo "
    ";
    }

    // line 125
    public function block_foot_script($context, array $blocks = array())
    {
        // line 126
        echo "    ";
        // line 128
        echo "    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"></script>
    <script>
        window.jQuery || document.write('<script src=\"../js/libs/jquery-1.7.2.min.js\"><\\/script>')
    </script>

    ";
        // line 135
        echo "    ";
        // line 137
        echo "    ";
        if ($this->getAttribute($this->getContext($context, "google", true), "analytics", array(), "array", true, true)) {
            // line 138
            echo "    <script>
        var _gaq = [['_setAccount', '";
            // line 139
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "google"), "analytics", array(), "array"), "html", null, true);
            echo "'], ['_trackPageview']];
        (function(d, t) {
            var g = d.createElement(t),
                s = d.getElementsByTagName(t)[0];
            g.async = g.src = '//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g, s);
        }(document, 'script'));
    </script>
    ";
        }
        // line 148
        echo "    ";
        $this->displayParentBlock("foot_script", $context, $blocks);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "MopaBootstrapBundle::base_initializr.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  338 => 138,  335 => 137,  333 => 135,  326 => 128,  321 => 125,  311 => 121,  304 => 118,  301 => 117,  297 => 125,  286 => 117,  275 => 112,  265 => 106,  261 => 104,  255 => 102,  237 => 44,  233 => 25,  216 => 21,  211 => 109,  206 => 105,  200 => 94,  188 => 86,  186 => 85,  180 => 83,  174 => 81,  153 => 74,  150 => 73,  129 => 65,  126 => 63,  81 => 39,  70 => 36,  53 => 15,  445 => 126,  441 => 115,  438 => 114,  424 => 114,  422 => 110,  419 => 109,  415 => 107,  412 => 106,  406 => 103,  401 => 101,  390 => 96,  385 => 93,  382 => 92,  376 => 90,  371 => 83,  363 => 78,  353 => 148,  349 => 80,  347 => 77,  344 => 76,  336 => 87,  334 => 75,  331 => 74,  328 => 73,  324 => 126,  317 => 67,  313 => 65,  310 => 64,  307 => 63,  295 => 59,  292 => 121,  289 => 120,  279 => 54,  276 => 53,  263 => 52,  259 => 104,  256 => 103,  251 => 99,  249 => 96,  244 => 92,  239 => 90,  236 => 89,  234 => 73,  231 => 72,  226 => 62,  223 => 57,  218 => 55,  215 => 53,  212 => 52,  197 => 90,  194 => 88,  192 => 106,  184 => 50,  181 => 47,  178 => 46,  165 => 78,  161 => 35,  152 => 32,  148 => 30,  146 => 23,  137 => 14,  127 => 36,  114 => 31,  90 => 5,  84 => 128,  77 => 38,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 116,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 100,  393 => 97,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 82,  365 => 111,  362 => 110,  360 => 77,  355 => 85,  341 => 139,  337 => 103,  322 => 101,  314 => 122,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 124,  285 => 89,  283 => 116,  278 => 113,  268 => 107,  264 => 84,  258 => 81,  252 => 101,  247 => 78,  241 => 91,  229 => 63,  220 => 70,  214 => 69,  177 => 82,  169 => 60,  140 => 55,  132 => 66,  128 => 49,  107 => 19,  61 => 13,  273 => 96,  269 => 94,  254 => 100,  243 => 96,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 23,  221 => 56,  219 => 22,  217 => 75,  208 => 68,  204 => 48,  179 => 69,  159 => 76,  143 => 22,  135 => 13,  119 => 42,  102 => 52,  71 => 19,  67 => 39,  63 => 26,  59 => 8,  38 => 3,  94 => 28,  89 => 20,  85 => 41,  75 => 17,  68 => 14,  56 => 17,  87 => 25,  21 => 2,  26 => 6,  93 => 44,  88 => 6,  78 => 21,  46 => 7,  27 => 4,  44 => 12,  31 => 5,  28 => 3,  201 => 47,  196 => 90,  183 => 82,  171 => 80,  166 => 71,  163 => 62,  158 => 34,  156 => 75,  151 => 63,  142 => 59,  138 => 68,  136 => 56,  121 => 33,  117 => 60,  105 => 55,  91 => 27,  62 => 9,  49 => 19,  24 => 4,  25 => 3,  19 => 1,  79 => 125,  72 => 43,  69 => 42,  47 => 9,  40 => 8,  37 => 10,  22 => 2,  246 => 97,  157 => 56,  145 => 46,  139 => 18,  131 => 52,  123 => 47,  120 => 61,  115 => 43,  111 => 37,  108 => 56,  101 => 10,  98 => 9,  96 => 31,  83 => 25,  74 => 45,  66 => 32,  55 => 15,  52 => 2,  50 => 14,  43 => 8,  41 => 4,  35 => 5,  32 => 4,  29 => 3,  209 => 106,  203 => 96,  199 => 67,  193 => 73,  189 => 105,  187 => 51,  182 => 66,  176 => 64,  173 => 43,  168 => 79,  164 => 59,  162 => 77,  154 => 58,  149 => 51,  147 => 71,  144 => 49,  141 => 69,  133 => 55,  130 => 41,  125 => 34,  122 => 43,  116 => 41,  112 => 57,  109 => 34,  106 => 36,  103 => 32,  99 => 48,  95 => 28,  92 => 21,  86 => 28,  82 => 126,  80 => 19,  73 => 37,  64 => 38,  60 => 21,  57 => 5,  54 => 10,  51 => 14,  48 => 13,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
