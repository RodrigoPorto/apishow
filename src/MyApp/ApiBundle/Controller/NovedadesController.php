<?php

namespace MyApp\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class NovedadesController extends Controller
{
    public function indexAction()
    {
        $ca = curl_init();
        curl_setopt($ca, CURLOPT_URL, "http://api.themoviedb.org/3/configuration?api_key=3f1a25ecca73c0013921750c6b7698e6");
        curl_setopt($ca, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ca, CURLOPT_HEADER, FALSE);
        curl_setopt($ca, CURLOPT_HTTPHEADER, array("Accept: application/json"));
        $response = curl_exec($ca);
        curl_close($ca);
        $config = json_decode($response, true);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.themoviedb.org//3/movie/now_playing?api_key=3f1a25ecca73c0013921750c6b7698e6");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json"));
        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response, true);


        $imagen0 = ($config['images']['base_url'] . $config['images']['poster_sizes'][2] . $result['results'][0]['poster_path']);

        $titulo0 = ($result['results'][0]['original_title']);


        $imagen2 = ($config['images']['base_url'] . $config['images']['poster_sizes'][2] . $result['results'][2]['poster_path']);

        $titulo2 = ($result['results'][2]['original_title']);


        $imagen3 = ($config['images']['base_url'] . $config['images']['poster_sizes'][2] . $result['results'][3]['poster_path']);

        $titulo3 = ($result['results'][3]['original_title']);

        $imagen4 = ($config['images']['base_url'] . $config['images']['poster_sizes'][2] . $result['results'][4]['poster_path']);

        $titulo4 = ($result['results'][4]['original_title']);

        $imagen5 = ($config['images']['base_url'] . $config['images']['poster_sizes'][2] . $result['results'][5]['poster_path']);

        $titulo5 = ($result['results'][5]['original_title']);

        $imagen6 = ($config['images']['base_url'] . $config['images']['poster_sizes'][2] . $result['results'][6]['poster_path']);

        $titulo6 = ($result['results'][6]['original_title']);


        return $this->render(
                    'ApiBundle:Default:index.html.twig',
                    array(
                        'imagen0'        => $imagen0,
                        'titulo0'         => $titulo0,
                        'imagen2'        => $imagen2,
                        'titulo2'         => $titulo2,
                        'imagen3'        => $imagen3,
                        'titulo3'         => $titulo3,
                        'imagen4'        => $imagen4,
                        'titulo4'         => $titulo4,
                        'imagen5'        => $imagen5,
                        'titulo5'         => $titulo5,
                        'imagen6'        => $imagen6,
                        'titulo6'         => $titulo6,
                
            )
        );
    }
}
