<?php

namespace MyApp\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SeriesController extends Controller
{
    public function indexAction()
    {
        return $this->render('ApiBundle:Series:index.html.twig',array(
                'accion'        => 0,
            ));
    }
    
    public function buscarAction()
    {
        if ($_POST['buscador'])
        { 
            $buscar = $_POST['serie'];
            $cadena = urlencode($buscar);

        if(empty($cadena))
        {
            
            echo "No se ha ingresado una cadena a buscar";
            
        }
        else
        {
            $ca = curl_init();
            curl_setopt($ca, CURLOPT_URL, "http://api.themoviedb.org/3/configuration?api_key=3f1a25ecca73c0013921750c6b7698e6");
            curl_setopt($ca, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ca, CURLOPT_HEADER, FALSE);
            curl_setopt($ca, CURLOPT_HTTPHEADER, array("Accept: application/json"));
            $response = curl_exec($ca);
            curl_close($ca);
            $config = json_decode($response, true);


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://api.themoviedb.org/3/search/tv?query={$cadena}&api_key=3f1a25ecca73c0013921750c6b7698e6");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json"));
            $response = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($response, true);


            $imagen= $config['images']['base_url'] . $config['images']['poster_sizes'][2] . 
                    $result['results'][0]['poster_path'];
            $id= ($result['results'][0]['id']);
            $titulo= $result['results'][0]['original_name'];
            $votos = ($result['results'][0]['vote_average']);


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://api.themoviedb.org/3/tv/{$id}?api_key=3f1a25ecca73c0013921750c6b7698e6&language=es");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json"));
            $response = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($response, true);
            
            $creador = ($result["created_by"][0]['name']);

            $temporadas = ($result["number_of_seasons"]);

            $descripcion = ($result['overview']);

        }
        }
        
            return $this->render(
                'ApiBundle:Series:index.html.twig',
                array(
                    'imagen'        => $imagen,
                    'titulo'        => $titulo,
                    'descripcion'   => $descripcion,
                    'votos'         => $votos,
                    'creador'       => $creador,
                    'temporadas'    =>  $temporadas,  
                    'accion'        => 1,
            )
        );
    }
}
