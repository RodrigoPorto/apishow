¿Que es ApiShow?
----------------------------------

ApiShow es una aplicación creada pensando en todos los los cinéﬁlos y adictos a las series. 
El objetivo de ApiShow es que el usuario pueda buscar películas y series y también dándole información de 
nuevos estrenos.

Funciones
----------------------------------
• Buscador de series y películas.

• Consulta de información.


Estructura
----------------------------------
La aplicación constará de dos páginas de Login.

• Una página de Login de Usuario,donde el usuario tendrá que loguearse para entrar en
la aplicación.

• Una página de Login de Admin,donde el administrador podrá gestionar la aplicación.

En la parte de la Aplicación,es donde el usuario podrá consultar información sobre
películas y series,comprobar si hay nuevos episodios y agregar nuevo contenido a su
sección de Favoritos.El usuario tendrá un menú con las siguientes opciones:

• Novedades.

• Búsqueda de películas y series.


Lenguajes
----------------------------------
En la aplicación de ApiShow se utilizaran diversos lenguajes:

• HTML/CSS: para el diseño de la página web se utiliza Bootstrap.

• Symfony: el proyecto estará basado en este popular framework.

• PHP: para consultar la información a la Api,realizar formularios,autenticación de los
usuarios...etc.

Api
----------------------------------
Para la consulta de información de películas y series,utilizaremos la Api The MovieDb.
https://www.themoviedb.org/documentation/api
A la aplicación le pediremos los siguientes parámetros:

• Titulo de la serie/película.

• Portada de la serie/película.

• Breve descripción.

---- Documentación ----

Para mas información consultar el archivo almacenado en Google Drive.